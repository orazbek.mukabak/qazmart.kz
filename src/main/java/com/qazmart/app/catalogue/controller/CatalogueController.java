package com.qazmart.app.catalogue.controller;

import com.qazmart.app.catalogue.entity.MainCatalogue;
import com.qazmart.app.catalogue.entity.SubCatalogue;
import com.qazmart.app.catalogue.service.CatalogueService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/catalogue")
public class CatalogueController {
    
    @Autowired
    private CatalogueService catalogueService;
    
    @GetMapping(value = "/read-main-catalogues")
    public @ResponseBody
    List readMainCatalogues(){
        List<MainCatalogue> catalogues = null;
        catalogues = catalogueService.readMainCatalogues();
        if(catalogues == null) {
            catalogues = new ArrayList<>();
        }
        return catalogues;
    }
    
    @GetMapping(path = "/read-main-catalogue/{id:.+}")
    public String readMainCatalogue(@PathVariable int id, Model model) {
        MainCatalogue mainCatalogue = catalogueService.readMainCatalogueById(id);
        if(mainCatalogue == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("mainCatalogue", mainCatalogue);
            return "catalogue/read-main-catalogue";
        }
    }
    
    @GetMapping(path = "/read-sub-catalogues-by-main-catalogue-id/{id:.+}")
    public @ResponseBody
    List readSubCataloguesByMainCatalogueId(@PathVariable int id){
        List<SubCatalogue> subCatalogues = null;
        subCatalogues = catalogueService.readSubCataloguesByMainCatalogueId(id);
        if(subCatalogues == null) {
            subCatalogues = new ArrayList<>();
        }
        return subCatalogues;
    }
    
    @GetMapping(path = "/read-sub-catalogue/{id:.+}")
    public String readSubCatalogue(@PathVariable int id, Model model) {
        SubCatalogue subCatalogue = catalogueService.readSubCatalogueById(id);
        if(subCatalogue == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("subCatalogue", subCatalogue);
            return "catalogue/read-sub-catalogue";
        }
    }
}
