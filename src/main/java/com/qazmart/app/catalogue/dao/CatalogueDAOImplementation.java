package com.qazmart.app.catalogue.dao;

import com.qazmart.app.catalogue.entity.MainCatalogue;
import com.qazmart.app.catalogue.entity.SubCatalogue;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CatalogueDAOImplementation implements CatalogueDAO {
    
    @Autowired
    private SessionFactory catalogueSessionFactory;

    @Override
    public List<MainCatalogue> readMainCatalogues() {
        Session currentSession = catalogueSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(MainCatalogue.class);
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("priority"), "priority"))
        .setResultTransformer(Transformers.aliasToBean(MainCatalogue.class));
        return query.list();
    }

    @Override
    public MainCatalogue readMainCatalogueById(int id) {
        Session currentSession = catalogueSessionFactory.getCurrentSession();
        return currentSession.get(MainCatalogue.class, id);
    }

    @Override
    public List<SubCatalogue> readSubCataloguesByMainCatalogueId(int id) {
        Session currentSession = catalogueSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(SubCatalogue.class);
        query.add(Restrictions.eq("mainCatalogueId", id));
        query.addOrder(Order.asc("priority"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("priority"), "priority")
        .add(Projections.property("mainCatalogueId"), "mainCatalogueId"))
        .setResultTransformer(Transformers.aliasToBean(SubCatalogue.class));
        return query.list();
    }

    @Override
    public SubCatalogue readSubCatalogueById(int id) {
        Session currentSession = catalogueSessionFactory.getCurrentSession();
        return currentSession.get(SubCatalogue.class, id);
    }
}
