package com.qazmart.app.catalogue.dao;

import com.qazmart.app.catalogue.entity.MainCatalogue;
import com.qazmart.app.catalogue.entity.SubCatalogue;
import java.util.List;

public interface CatalogueDAO {
    public List<MainCatalogue> readMainCatalogues();
    public MainCatalogue readMainCatalogueById(int id);
    public List<SubCatalogue> readSubCataloguesByMainCatalogueId(int id);
    public SubCatalogue readSubCatalogueById(int id);
}
