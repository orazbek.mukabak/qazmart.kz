package com.qazmart.app.catalogue.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="sub_catalogue")
public class SubCatalogue implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="priority")
    private double priority;
    @Column(name="main_catalogue_id")
    private int mainCatalogueId;

    public SubCatalogue() {
    }

    public SubCatalogue(String title, double priority, int mainCatalogueId) {
        this.title = title;
        this.priority = priority;
        this.mainCatalogueId = mainCatalogueId;
    }
    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPriority() {
        return priority;
    }

    public void setPriority(double priority) {
        this.priority = priority;
    }

    public int getMainCatalogueId() {
        return mainCatalogueId;
    }

    public void setMainCatalogueId(int mainCatalogueId) {
        this.mainCatalogueId = mainCatalogueId;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}