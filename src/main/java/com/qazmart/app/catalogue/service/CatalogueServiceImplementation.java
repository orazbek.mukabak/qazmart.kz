package com.qazmart.app.catalogue.service;

import com.qazmart.app.catalogue.entity.MainCatalogue;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qazmart.app.catalogue.dao.CatalogueDAO;
import com.qazmart.app.catalogue.entity.SubCatalogue;

@Service
public class CatalogueServiceImplementation implements CatalogueService {
    
    @Autowired
    private CatalogueDAO catalogueDAO;

    @Override
    @Transactional("catalogueTransactionManager")
    public List <MainCatalogue> readMainCatalogues() {
        return catalogueDAO.readMainCatalogues();
    }

    @Override
    @Transactional("catalogueTransactionManager")
    public MainCatalogue readMainCatalogueById(int id) {
        return catalogueDAO.readMainCatalogueById(id);
    }

    @Override
    @Transactional("catalogueTransactionManager")
    public List<SubCatalogue> readSubCataloguesByMainCatalogueId(int id) {
        return catalogueDAO.readSubCataloguesByMainCatalogueId(id);
    }

    @Override
    @Transactional("catalogueTransactionManager")
    public SubCatalogue readSubCatalogueById(int id) {
        return catalogueDAO.readSubCatalogueById(id);
    }
}

