package com.qazmart.app.auth.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/")
    public String displayIndexPage() {
        return "index";
    }
    
    @GetMapping("/logout")
    public String logoutPage (HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){   
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }
    
    @GetMapping("/display-banner")
    public String displayBannerPage() {
        return "banner";
    }
    
    @GetMapping("/display-footer")
    public String displayFooterPage() {
        return "footer";
    }
    
    @GetMapping("/display-about-us")
    public String displayAboutUsPage() {
        return "about-us";
    }
    
    @GetMapping("/display-privacy")
    public String displayPrivacyPage() {
        return "privacy";
    }
    
    @GetMapping("/display-bank-statement")
    public String displayBankStatementPage() {
        return "bank-statement";
    }
    
    @GetMapping("/display-offer")
    public String displayOfferPage() {
        return "offer";
    }

    @GetMapping("/signin")
    public String displaySigninPage(@RequestParam("u") String u, @RequestParam("p") String p) {
        return "signin";
    }
    
    @GetMapping("/test")
    public String test() {
        return "test";
    }
    
    @GetMapping("/confirm-recovery")
    public String displayConfirmRecoveryPage(@RequestParam("u") String u, @RequestParam("p") String p) {
        return "confirm-recovery";
    }

    @GetMapping("/exception403")
    public String displayException403() {
        return "exception/exception403";
    }
    
    @GetMapping("/exception404")
    public String displayException404() {
        return "exception/exception404";
    }
}
