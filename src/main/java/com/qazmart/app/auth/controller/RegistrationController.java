package com.qazmart.app.auth.controller;

import com.qazmart.app.auth.entity.AuthUser;
import com.qazmart.app.profile.entity.Profile;
import com.qazmart.app.profile.service.ProfileService;
import com.qazmart.app.sms.controller.Smsc;
import com.qazmart.app.sms.entity.PasswordRecovery;
import com.qazmart.app.sms.service.SmsService;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/register")
public class RegistrationController {
    
    @Autowired
    private UserDetailsManager userDetailsManager;
    private PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    
    @Autowired
    private SmsService smsService;
    
    @Autowired
    private ProfileService profileService;
    
    @InitBinder
    public void initBinder(WebDataBinder dataBinder) {
        StringTrimmerEditor stringTrimmerEditor = new StringTrimmerEditor(true);
        dataBinder.registerCustomEditor(String.class, stringTrimmerEditor);
    }	

    @GetMapping("/signup")
    public String displaySignupPage(Model theModel) {
        theModel.addAttribute("authUser", new AuthUser());
        return "signup";
    }

    @PostMapping("/signing")
    public String signingProcess (
        @Valid @ModelAttribute("authUser") AuthUser theAuthUser, BindingResult theBindingResult, Model theModel) {
        String username = theAuthUser.getUserName();
        
        if(username == null) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_EMPTY_USERNAME");
            return "signup";	
        }

        if(theBindingResult.hasErrors()) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_WRONG_PATTERN");
            return "signup";
        }

        boolean userExists = doesUserExist(username);
        if(userExists) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_USER_ALREADY_EXISTS");
            return "signup";			
        }
        String pass = theAuthUser.getPassword();
        String encodedPassword = passwordEncoder.encode(pass);
        encodedPassword = "{bcrypt}" + encodedPassword;
        //List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("STUDENT", "TUTOR", "MANAGER");
        List<GrantedAuthority> authorities = AuthorityUtils.createAuthorityList("CUSTOMER");
        User tempUser = new User(username, encodedPassword, authorities);
        userDetailsManager.createUser(tempUser);
        Profile profile = profileService.createProfile(username);
        String redirectURL = "redirect:/signin?u=" + username + "&p=" + pass;
        return redirectURL;
    }

    private boolean doesUserExist(String username) {
        boolean exists = userDetailsManager.userExists(username);
        return exists;
    }
    
    @GetMapping("/recovery")
    public String displayRecoveryPage(Model theModel) {
        theModel.addAttribute("authUser", new AuthUser());
        return "recovery";
    }
    
    @PostMapping("/recoverying")
    public String recoveryingProcess (
        @Valid @ModelAttribute("authUser") AuthUser theAuthUser, BindingResult theBindingResult, Model theModel) {
        String username = theAuthUser.getUserName();
        String pass = theAuthUser.getPassword();
        
        if(username == null) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_EMPTY_USERNAME");
            return "recovery";	
        }
        
        if(pass == null) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_EMPTY_PASSWORD");
            return "recovery";	
        }

        if(pass.length() < 6 || pass.length() > 32) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_WRONG_PATTERN");
            return "recovery";
        }
        
        boolean userExists = doesUserExist(username);
        if(!userExists) {
            theModel.addAttribute("authUser", new AuthUser());
            theModel.addAttribute("registrationError", "ERROR_USER_DO_NOT_EXISTS");
            return "recovery";			
        }
        
        int code = 1000 + (int)(Math.random() * ((9999 - 1000) + 1));
        PasswordRecovery passwordRecovery = new PasswordRecovery(username, code);
        smsService.savePasswordRecovery(passwordRecovery);
        sendGET(username, code);
        return "redirect:/confirm-recovery?u=" + username + "&p=" + pass;
    }
    
    @GetMapping(path = "/recovery-confirming")
    public String recoveryConfirmingProcess(@RequestParam("code") int code, @RequestParam("phone") String phone, @RequestParam("password") String password) {
        PasswordRecovery passwordRecovery = smsService.findPasswordRecovery(phone);
        if(passwordRecovery.getCode() == code) {
            String encodedPassword = passwordEncoder.encode(password);
            encodedPassword = "{bcrypt}" + encodedPassword;
            UserDetails  userDetails = userDetailsManager.loadUserByUsername(phone);
            String authoritiesString = "" + userDetails.getAuthorities();
            authoritiesString = authoritiesString.substring(1, authoritiesString.length() - 1);
            List<GrantedAuthority> authorities = AuthorityUtils.commaSeparatedStringToAuthorityList(authoritiesString);
            User tempUser = new User(phone, encodedPassword, authorities);
            userDetailsManager.updateUser(tempUser);
            return "MESSAGE_SUCCESSFUL";
        } else {
            return "MESSAGE_FAIL";
        }
    }
    
    private static void sendGET(String username, int code) {
	Smsc sd = new Smsc();
        String message = "SMS Kod: " + code;
        sd.send_sms(username, message, 1, "", "", 0, "OZAT.ONLINE", "");
    }
}