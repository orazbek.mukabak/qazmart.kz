package com.qazmart.app.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.qazmart.app")
@PropertySource({"classpath:cart-persistence-mysql.properties"})
public class ConfigCart implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="cartDataSource")
    public DataSource cartDataSource() {
        ComboPooledDataSource cartDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("cart.localhost.hostname");
        String localHostName2 = env.getProperty("cart.localhost.hostname2");
        try {
            cartDataSource.setDriverClass(env.getProperty("cart.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigCart.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            cartDataSource.setJdbcUrl(env.getProperty("cart.jdbc.url"));
        } else {
            cartDataSource.setJdbcUrl(env.getProperty("cart.google.cloud.jdbc.url"));
        }
        cartDataSource.setUser(env.getProperty("cart.jdbc.user"));
        cartDataSource.setPassword(env.getProperty("cart.jdbc.password"));
        cartDataSource.setInitialPoolSize(getIntProperty("cart.connection.pool.initialPoolSize"));
        cartDataSource.setMinPoolSize(getIntProperty("cart.connection.pool.minPoolSize"));
        cartDataSource.setMaxPoolSize(getIntProperty("cart.connection.pool.maxPoolSize"));		
        cartDataSource.setMaxIdleTime(getIntProperty("cart.connection.pool.maxIdleTime"));
        return cartDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("cart.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("cart.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="cartSessionFactory")
    public LocalSessionFactoryBean cartSessionFactory() {
        LocalSessionFactoryBean cartSessionFactory = new LocalSessionFactoryBean();
        cartSessionFactory.setDataSource(cartDataSource());
        cartSessionFactory.setPackagesToScan(env.getProperty("cart.hibernate.packagesToScan"));
        cartSessionFactory.setHibernateProperties(getHibernateProperties());
        return cartSessionFactory;
    }

    @Bean(name="cartTransactionManager")
    @Autowired
    public HibernateTransactionManager cartTransactionManager(SessionFactory cartSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(cartSessionFactory);
        return txManager;
    }
}