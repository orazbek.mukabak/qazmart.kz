package com.qazmart.app.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.qazmart.app")
@PropertySource({"classpath:catalogue-persistence-mysql.properties"})
public class ConfigCatalogue implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="catalogueDataSource")
    public DataSource catalogueDataSource() {
        ComboPooledDataSource catalogueDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("catalogue.localhost.hostname");
        String localHostName2 = env.getProperty("catalogue.localhost.hostname2");
        try {
            catalogueDataSource.setDriverClass(env.getProperty("catalogue.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigCatalogue.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            catalogueDataSource.setJdbcUrl(env.getProperty("catalogue.jdbc.url"));
        } else {
            catalogueDataSource.setJdbcUrl(env.getProperty("catalogue.google.cloud.jdbc.url"));
        }
        catalogueDataSource.setUser(env.getProperty("catalogue.jdbc.user"));
        catalogueDataSource.setPassword(env.getProperty("catalogue.jdbc.password"));
        catalogueDataSource.setInitialPoolSize(getIntProperty("catalogue.connection.pool.initialPoolSize"));
        catalogueDataSource.setMinPoolSize(getIntProperty("catalogue.connection.pool.minPoolSize"));
        catalogueDataSource.setMaxPoolSize(getIntProperty("catalogue.connection.pool.maxPoolSize"));		
        catalogueDataSource.setMaxIdleTime(getIntProperty("catalogue.connection.pool.maxIdleTime"));
        return catalogueDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("catalogue.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("catalogue.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="catalogueSessionFactory")
    public LocalSessionFactoryBean catalogueSessionFactory() {
        LocalSessionFactoryBean catalogueSessionFactory = new LocalSessionFactoryBean();
        catalogueSessionFactory.setDataSource(catalogueDataSource());
        catalogueSessionFactory.setPackagesToScan(env.getProperty("catalogue.hibernate.packagesToScan"));
        catalogueSessionFactory.setHibernateProperties(getHibernateProperties());
        return catalogueSessionFactory;
    }

    @Bean(name="catalogueTransactionManager")
    @Autowired
    public HibernateTransactionManager catalogueTransactionManager(SessionFactory catalogueSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(catalogueSessionFactory);
        return txManager;
    }
}