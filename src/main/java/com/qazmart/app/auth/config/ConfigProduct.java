package com.qazmart.app.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.qazmart.app")
@PropertySource({"classpath:product-persistence-mysql.properties"})
public class ConfigProduct implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="productDataSource")
    public DataSource productDataSource() {
        ComboPooledDataSource productDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("product.localhost.hostname");
        String localHostName2 = env.getProperty("product.localhost.hostname2");
        try {
            productDataSource.setDriverClass(env.getProperty("product.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            productDataSource.setJdbcUrl(env.getProperty("product.jdbc.url"));
        } else {
            productDataSource.setJdbcUrl(env.getProperty("product.google.cloud.jdbc.url"));
        }
        productDataSource.setUser(env.getProperty("product.jdbc.user"));
        productDataSource.setPassword(env.getProperty("product.jdbc.password"));
        productDataSource.setInitialPoolSize(getIntProperty("product.connection.pool.initialPoolSize"));
        productDataSource.setMinPoolSize(getIntProperty("product.connection.pool.minPoolSize"));
        productDataSource.setMaxPoolSize(getIntProperty("product.connection.pool.maxPoolSize"));		
        productDataSource.setMaxIdleTime(getIntProperty("product.connection.pool.maxIdleTime"));
        return productDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("product.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("product.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="productSessionFactory")
    public LocalSessionFactoryBean productSessionFactory() {
        LocalSessionFactoryBean productSessionFactory = new LocalSessionFactoryBean();
        productSessionFactory.setDataSource(productDataSource());
        productSessionFactory.setPackagesToScan(env.getProperty("product.hibernate.packagesToScan"));
        productSessionFactory.setHibernateProperties(getHibernateProperties());
        return productSessionFactory;
    }

    @Bean(name="productTransactionManager")
    @Autowired
    public HibernateTransactionManager productTransactionManager(SessionFactory productSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(productSessionFactory);
        return txManager;
    }
}