package com.qazmart.app.auth.config;

import java.beans.PropertyVetoException;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import com.mchange.v2.c3p0.ComboPooledDataSource;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.qazmart.app")
@PropertySource({"classpath:payment-persistence-mysql.properties"})
public class ConfigPayment implements WebMvcConfigurer {
    @Autowired
    private Environment env;
	
    @Bean(name="paymentDataSource")
    public DataSource paymentDataSource() {
        ComboPooledDataSource paymentDataSource = new ComboPooledDataSource();
        String runningHostName = "";
        String localHostName = env.getProperty("payment.localhost.hostname");
        String localHostName2 = env.getProperty("payment.localhost.hostname2");
        try {
            paymentDataSource.setDriverClass(env.getProperty("payment.jdbc.driver"));
            runningHostName = InetAddress.getLocalHost().getHostName();
        } catch (PropertyVetoException exc) {
            throw new RuntimeException(exc);
        } catch (UnknownHostException ex) {
            Logger.getLogger(ConfigPayment.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(runningHostName.equals(localHostName) || runningHostName.equals(localHostName2)) {
            paymentDataSource.setJdbcUrl(env.getProperty("payment.jdbc.url"));
        } else {
            paymentDataSource.setJdbcUrl(env.getProperty("payment.google.cloud.jdbc.url"));
        }
        paymentDataSource.setUser(env.getProperty("payment.jdbc.user"));
        paymentDataSource.setPassword(env.getProperty("payment.jdbc.password"));
        paymentDataSource.setInitialPoolSize(getIntProperty("payment.connection.pool.initialPoolSize"));
        paymentDataSource.setMinPoolSize(getIntProperty("payment.connection.pool.minPoolSize"));
        paymentDataSource.setMaxPoolSize(getIntProperty("payment.connection.pool.maxPoolSize"));		
        paymentDataSource.setMaxIdleTime(getIntProperty("payment.connection.pool.maxIdleTime"));
        return paymentDataSource;
    }
    
    private Properties getHibernateProperties() {
        Properties props = new Properties();
        props.setProperty("hibernate.dialect", env.getProperty("payment.hibernate.dialect"));
        props.setProperty("hibernate.show_sql", env.getProperty("payment.hibernate.show_sql"));
        return props;				
    }
    
    private int getIntProperty(String propName) {
        return Integer.parseInt(env.getProperty(propName));
    }	

    @Bean(name="paymentSessionFactory")
    public LocalSessionFactoryBean paymentSessionFactory() {
        LocalSessionFactoryBean paymentSessionFactory = new LocalSessionFactoryBean();
        paymentSessionFactory.setDataSource(paymentDataSource());
        paymentSessionFactory.setPackagesToScan(env.getProperty("payment.hibernate.packagesToScan"));
        paymentSessionFactory.setHibernateProperties(getHibernateProperties());
        return paymentSessionFactory;
    }

    @Bean(name="paymentTransactionManager")
    @Autowired
    public HibernateTransactionManager paymentTransactionManager(SessionFactory paymentSessionFactory) {
        HibernateTransactionManager txManager = new HibernateTransactionManager();
        txManager.setSessionFactory(paymentSessionFactory);
        return txManager;
    }
}