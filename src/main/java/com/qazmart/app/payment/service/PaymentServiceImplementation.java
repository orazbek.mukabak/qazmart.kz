package com.qazmart.app.payment.service;

import com.qazmart.app.payment.dao.PaymentDAO;
import com.qazmart.app.payment.entity.Account;
import com.qazmart.app.payment.entity.Purchase;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class PaymentServiceImplementation implements PaymentService {

    @Autowired
    private PaymentDAO paymentDAO;

    @Override
    @Transactional("paymentTransactionManager")
    public Purchase findPurchaseByCourseIdAndUsername(int courseId, String username) {
        return paymentDAO.findPurchaseByCourseIdAndUsername(courseId, username);
    }
    
    @Override
    @Transactional("paymentTransactionManager")
    public String save(Account account) {
        return paymentDAO.save(account);
    }

    @Override
    @Transactional("paymentTransactionManager")
    public Account findAccountByInnerId(int innerId) {
        Account account = paymentDAO.findAccountByInnerId(innerId);
        if(account == null) {
            save(new Account(innerId, 0));
            account = paymentDAO.findAccountByInnerId(innerId);
        }
        return account;
    }

    @Override
    @Transactional("paymentTransactionManager")
    public String save(Purchase purchase) {
        return paymentDAO.save(purchase);
    }

    @Override
    @Transactional("paymentTransactionManager")
    public List<Purchase> findPurchasesByCourseId(int courseId) {
        return paymentDAO.findPurchasesByCourseId(courseId);
    }

    @Override
    @Transactional("paymentTransactionManager")
    public List<Purchase> findPurchasesByUsername(String username) {
        return paymentDAO.findPurchasesByUsername(username);
    }
}