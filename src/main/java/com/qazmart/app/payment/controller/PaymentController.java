package com.qazmart.app.payment.controller;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import com.qazmart.app.payment.entity.Account;
import com.qazmart.app.payment.entity.Purchase;
import com.qazmart.app.payment.service.PaymentService;
import com.qazmart.app.profile.entity.Profile;
import com.qazmart.app.profile.service.ProfileService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private PaymentService paymentService;
//    
//    @Autowired
//    private CourseService courseService;
    
    @Autowired
    private ProfileService profileService;
    
    @GetMapping(value = "/get-purchase-status/")
    public String getPurchaseStatus(@RequestParam("courseId") int courseId, Model model){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        if(username.equals("anonymousUser")) {
            model.addAttribute("command", "COMMAND.REFRESH");
            return "command";
        } else {
            Purchase purchase = paymentService.findPurchaseByCourseIdAndUsername(courseId, username);
            if(purchase == null) {
                model.addAttribute("status", "STATUS.NOT_PAID");
                return "status";
            } else {
                model.addAttribute("status", "STATUS.PAID");
                return "status";
            }
        }
    }
    
//    @GetMapping(value = "/purchase-course/{id:.+}")
//    public String purchaseCourseProcess(@PathVariable("id") int id, Model model){
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String username = authentication.getName();
//        if(username.equals("anonymousUser")) {
//            model.addAttribute("command", "COMMAND.REFRESH");
//            return "command";
//        } else {
//            Profile profile = profileService.findProfileById(username);
//            Account account = paymentService.findAccountByInnerId(profile.getInnerId());
//            Course course = courseService.findCourseById(id);
//            if(account.getBalance() < course.getPrice()) {
//                double rest = 0;
//                rest = course.getPrice() - account.getBalance();
//                model.addAttribute("status", rest + "_STATUS.NOT_ENOUGH_BALANCE");
//                return "status";
//            } else {
//                account.setBalance(account.getBalance() - course.getPrice());
//                paymentService.save(account);
//                Purchase purchase = new Purchase("C" + id, username, new Date());
//                paymentService.save(purchase);
//                model.addAttribute("status", "STATUS.SUCCESSFUL");
//                return "status";
//            }
//        }
//    }
    
//    @GetMapping(value = "/update-undefined-error/{id:.+}")
//    public String updateUndefinedErrorProcess(@PathVariable("id") int id, @RequestParam("errorAmount") double errorAmount, Model model){
//        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
//        String username = authentication.getName();
//        if(username.equals("anonymousUser")) {
//            model.addAttribute("command", "COMMAND.REFRESH");
//            return "command";
//        } else {
//            Profile profile = profileService.findProfileById(username);
//            // Second try get profile
//            if(profile == null) {
//                profile = profileService.findProfileById(username);
//            }
//
//            Account account = paymentService.findAccountByInnerId(profile.getInnerId());
//            // Second try get account
//            if(account == null) {
//                account = paymentService.findAccountByInnerId(profile.getInnerId());
//            }
//
//            account.setBalance(account.getBalance() + errorAmount);
//            paymentService.save(account);
//            account = null;
//
//            Course course = courseService.findCourseById(id);
//            // Second try get course
//            if(course == null) {
//                course = courseService.findCourseById(id);
//            }
//
//            account = paymentService.findAccountByInnerId(profile.getInnerId());
//            // Second try get account
//            if(account == null) {
//                account = paymentService.findAccountByInnerId(profile.getInnerId());
//            }
//
//            if(account.getBalance() < course.getPrice()) {
//                double rest = 0;
//                rest = course.getPrice() - account.getBalance();
//                model.addAttribute("status", rest + "_STATUS.NOT_ENOUGH_BALANCE");
//                return "status";
//            } else {
//                account.setBalance(account.getBalance() - course.getPrice());
//                paymentService.save(account);
//                Date purchasedDate = new Date();
//                Purchase purchase = new Purchase("C" + id, username, purchasedDate);
//                paymentService.save(purchase);
//                model.addAttribute("status", "STATUS.SUCCESSFUL");
//                return "status";
//            }
//        }
//    }
    
    @GetMapping(value = "/get-purchases-by-username/")
    public @ResponseBody
    List getPurchasesByUsername(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        return paymentService.findPurchasesByUsername(username);
    }
    
    @GetMapping(path="/offer")
    public String sendTelegramMessage(@RequestParam("mobile") String mobile, @RequestParam("name") String name, Model model) {
        TelegramBot bot = new TelegramBot("1187575630:AAFkNYMmIwb1N6uE5UtOgMye8ILY3maqIA0");
        bot.setUpdatesListener(updates -> {
            return UpdatesListener.CONFIRMED_UPDATES_ALL;
        });
        String message = name + " " + mobile;
        SendResponse response = bot.execute(new SendMessage(-307305224, message));
        model.addAttribute("status", "STATUS.SUCCESSFUL");
        return "status";
    }
}