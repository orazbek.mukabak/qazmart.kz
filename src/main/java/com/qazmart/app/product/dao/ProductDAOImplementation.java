package com.qazmart.app.product.dao;

import com.qazmart.app.product.entity.Product;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class ProductDAOImplementation implements ProductDAO {
    
    @Autowired
    private SessionFactory productSessionFactory;

    @Override
    public List<Product> readProducts() {
        Session currentSession = productSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Product.class);
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("mainCatalogueId"), "mainCatalogueId")
        .add(Projections.property("subCatalogueId"), "subCatalogueId")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discount"), "discount")
        .add(Projections.property("imgUrl"), "imgUrl")
        .add(Projections.property("d1t"), "d1t")
        .add(Projections.property("d1v"), "d1v")
        .add(Projections.property("d2t"), "d2t")
        .add(Projections.property("d2v"), "d2v")
        .add(Projections.property("d3t"), "d3t")
        .add(Projections.property("d3v"), "d3v")
        .add(Projections.property("fullDescription"), "fullDescription")
        .add(Projections.property("owner"), "owner")
        .add(Projections.property("createdDate"), "createdDate")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Product.class));
        return query.list();
    }

    @Override
    public String create(Product product) {
        Session currentSession = productSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(product);
        return "STATUS.OK";
    }

    @Override
    public List<Product> readProductsByMainCatalogueId(int id) {
        Session currentSession = productSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Product.class);
        query.add(Restrictions.eq("mainCatalogueId", id));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("mainCatalogueId"), "mainCatalogueId")
        .add(Projections.property("subCatalogueId"), "subCatalogueId")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discount"), "discount")
        .add(Projections.property("imgUrl"), "imgUrl")
        .add(Projections.property("d1t"), "d1t")
        .add(Projections.property("d1v"), "d1v")
        .add(Projections.property("d2t"), "d2t")
        .add(Projections.property("d2v"), "d2v")
        .add(Projections.property("d3t"), "d3t")
        .add(Projections.property("d3v"), "d3v")
        .add(Projections.property("fullDescription"), "fullDescription")
        .add(Projections.property("owner"), "owner")
        .add(Projections.property("createdDate"), "createdDate")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Product.class));
        return query.list();
    }

    @Override
    public List<Product> readProductsByMainSubCatalogueId(int id, int subCatalogueId) {
        Session currentSession = productSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Product.class);
        query.add(Restrictions.eq("mainCatalogueId", id));
        query.add(Restrictions.eq("subCatalogueId", subCatalogueId));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("mainCatalogueId"), "mainCatalogueId")
        .add(Projections.property("subCatalogueId"), "subCatalogueId")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discount"), "discount")
        .add(Projections.property("imgUrl"), "imgUrl")
        .add(Projections.property("d1t"), "d1t")
        .add(Projections.property("d1v"), "d1v")
        .add(Projections.property("d2t"), "d2t")
        .add(Projections.property("d2v"), "d2v")
        .add(Projections.property("d3t"), "d3t")
        .add(Projections.property("d3v"), "d3v")
        .add(Projections.property("fullDescription"), "fullDescription")
        .add(Projections.property("owner"), "owner")
        .add(Projections.property("createdDate"), "createdDate")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Product.class));
        return query.list();
    }

    @Override
    public Product readProductById(int id) {
        Session currentSession = productSessionFactory.getCurrentSession();
        return currentSession.get(Product.class, id);
    }
    
    @Override
    public String deleteProduct(int id) {
        Session currentSession = productSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Product where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Product is deleted!";
    }
    
    @Override
    public List<Product> readProductsByKeyword(String keyWord) {
        Session currentSession = productSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Product.class);
        query.add(Restrictions.like("title", keyWord, MatchMode.ANYWHERE));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("title"), "title")
        .add(Projections.property("mainCatalogueId"), "mainCatalogueId")
        .add(Projections.property("subCatalogueId"), "subCatalogueId")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discount"), "discount")
        .add(Projections.property("imgUrl"), "imgUrl")
        .add(Projections.property("d1t"), "d1t")
        .add(Projections.property("d1v"), "d1v")
        .add(Projections.property("d2t"), "d2t")
        .add(Projections.property("d2v"), "d2v")
        .add(Projections.property("d3t"), "d3t")
        .add(Projections.property("d3v"), "d3v")
        .add(Projections.property("fullDescription"), "fullDescription")
        .add(Projections.property("owner"), "owner")
        .add(Projections.property("createdDate"), "createdDate")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Product.class));
        return query.list();
    }
}
