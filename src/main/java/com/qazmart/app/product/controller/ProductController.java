package com.qazmart.app.product.controller;

import com.qazmart.app.product.entity.Product;
import com.qazmart.app.product.service.ProductService;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/product")
public class ProductController {
    
    @Autowired
    private ProductService productService;
    
    @GetMapping(value = "/read-products-jsp")
    public String readProductsJsp(){
        return "product/read-products";
    }
    
    @GetMapping(value = "/read-products")
    public @ResponseBody
    List readProducts(){
        return productService.readProducts();
    }
    
    @GetMapping(value = "/create-product")
    public String createProduct(){
        return "product/create-product";
    }
    
    @PostMapping(value = "/create-product-process")
    @ResponseBody
    public String createProductProcess(@RequestBody Product product) {
        Date createdDate = new Date();
        product.setCreatedDate(createdDate);
        return productService.create(product);
    }
    
    @GetMapping(path = "/read-products-by-main-catalogue-id/{id:.+}")
    public @ResponseBody
    List readProductsByMainCatalogueId(@PathVariable int id){
        return productService.readProductsByMainCatalogueId(id);
    }
    
    @GetMapping(path = "/read-products-by-main-sub-catalogue-id/{id:.+}")
    public @ResponseBody
    List readProductsByMainSubCatalogueId(@PathVariable int id, @RequestParam("subCatalogueId") int subCatalogueId){
        return productService.readProductsByMainSubCatalogueId(id, subCatalogueId);
    }
    
    @GetMapping(path = "/read-product/{id:.+}")
    public String readProduct(@PathVariable int id, Model model){
        Product product = productService.readProductById(id);
        if(product == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("product", product);
            return "product/read-product";
        }
    }
    
    @GetMapping(path = "/update-product-image/{id:.+}")
    public String updateProductImage(@PathVariable int id, String imgUrl, Model model){
        Product product = productService.readProductById(id);
        product.setImgUrl(imgUrl);
        productService.create(product);
        model.addAttribute("status", "STATUS.SUCCESS");
        return "status";
    }
    
    @GetMapping(path="/delete-product/{id:.+}")
    public String deleteProduct(@PathVariable int id, Model model) {
        String status = productService.deleteProduct(id);
        model.addAttribute("status", status);
        return "status";
    }
    
    @GetMapping(path = "/update-status/{id:.+}")
    public String updateStatus(@PathVariable int id, @RequestParam("status") char status, Model model) {
        Product product = productService.readProductById(id);
        product.setStatus(status);
        String output = productService.create(product);
        model.addAttribute("status", output);
        return "status";
    }
    
    @GetMapping(value = "/read-products-by-keyword")
    public @ResponseBody
    List readProductsByKeyword(@RequestParam("keyword") String keyword){
        return productService.readProductsByKeyword(keyword);
    }
}
