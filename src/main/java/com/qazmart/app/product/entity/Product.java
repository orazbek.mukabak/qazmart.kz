package com.qazmart.app.product.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="product")
public class Product implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="title")
    private String title;
    @Column(name="main_catalogue_id")
    private int mainCatalogueId;
    @Column(name="sub_catalogue_id")
    private int subCatalogueId;
    @Column(name="price")
    private double price;
    @Column(name="discount")
    private int discount;
    @Column(name="img_url")
    private String imgUrl;
    @Column(name="d1t")
    private String d1t;
    @Column(name="d1v")
    private String d1v;
    @Column(name="d2t")
    private String d2t;
    @Column(name="d2v")
    private String d2v;
    @Column(name="d3t")
    private String d3t;
    @Column(name="d3v")
    private String d3v;
    @Column(name="full_description")
    private String fullDescription;
    @Column(name="owner")
    private String owner;
    @Column(name="created_date")
    private Date createdDate;
    @Column(name="status")
    private char status;

    public Product() {
    }

    public Product(String title, int mainCatalogueId, int subCatalogueId, double price, int discount, String imgUrl, String d1t, String d1v, String d2t, String d2v, String d3t, String d3v, String fullDescription, String owner, Date createdDate, char status) {
        this.title = title;
        this.mainCatalogueId = mainCatalogueId;
        this.subCatalogueId = subCatalogueId;
        this.price = price;
        this.discount = discount;
        this.imgUrl = imgUrl;
        this.d1t = d1t;
        this.d1v = d1v;
        this.d2t = d2t;
        this.d2v = d2v;
        this.d3t = d3t;
        this.d3v = d3v;
        this.fullDescription = fullDescription;
        this.owner = owner;
        this.createdDate = createdDate;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMainCatalogueId() {
        return mainCatalogueId;
    }

    public void setMainCatalogueId(int mainCatalogueId) {
        this.mainCatalogueId = mainCatalogueId;
    }

    public int getSubCatalogueId() {
        return subCatalogueId;
    }

    public void setSubCatalogueId(int subCatalogueId) {
        this.subCatalogueId = subCatalogueId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getD1t() {
        return d1t;
    }

    public void setD1t(String d1t) {
        this.d1t = d1t;
    }

    public String getD1v() {
        return d1v;
    }

    public void setD1v(String d1v) {
        this.d1v = d1v;
    }

    public String getD2t() {
        return d2t;
    }

    public void setD2t(String d2t) {
        this.d2t = d2t;
    }

    public String getD2v() {
        return d2v;
    }

    public void setD2v(String d2v) {
        this.d2v = d2v;
    }

    public String getD3t() {
        return d3t;
    }

    public void setD3t(String d3t) {
        this.d3t = d3t;
    }

    public String getD3v() {
        return d3v;
    }

    public void setD3v(String d3v) {
        this.d3v = d3v;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}