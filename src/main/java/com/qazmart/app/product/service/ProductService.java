package com.qazmart.app.product.service;

import com.qazmart.app.product.entity.Product;
import java.util.List;

public interface ProductService {
    public List<Product> readProducts();
    public String create(Product product);
    public List<Product> readProductsByMainCatalogueId(int id);
    public List<Product> readProductsByMainSubCatalogueId(int id, int subCatalogueId);
    public Product readProductById(int id);
    public String deleteProduct(int id);
    public List<Product> readProductsByKeyword(String keyWord);
}
