package com.qazmart.app.product.service;

import com.qazmart.app.product.entity.Product;
import com.qazmart.app.product.dao.ProductDAO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ProductServiceImplementation implements ProductService {
    
    @Autowired
    private ProductDAO productDAO;

    @Override
    @Transactional("productTransactionManager")
    public List <Product> readProducts() {
        return productDAO.readProducts();
    }

    @Override
    @Transactional("productTransactionManager")
    public String create(Product product) {
        return productDAO.create(product);
    }

    @Override
    @Transactional("productTransactionManager")
    public List<Product> readProductsByMainCatalogueId(int id) {
        return productDAO.readProductsByMainCatalogueId(id);
    }

    @Override
    @Transactional("productTransactionManager")
    public List<Product> readProductsByMainSubCatalogueId(int id, int subCatalogueId) {
        return productDAO.readProductsByMainSubCatalogueId(id, subCatalogueId);
    }

    @Override
    @Transactional("productTransactionManager")
    public Product readProductById(int id) {
        return productDAO.readProductById(id);
    }

    @Override
    @Transactional("productTransactionManager")
    public String deleteProduct(int id) {
        return productDAO.deleteProduct(id);
    }
    
    @Override
    @Transactional("productTransactionManager")
    public List<Product> readProductsByKeyword(String keyWord) {
        return productDAO.readProductsByKeyword(keyWord);
    }
}
