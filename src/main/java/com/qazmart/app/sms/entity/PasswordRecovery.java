package com.qazmart.app.sms.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="password_recovery")
public class PasswordRecovery implements Serializable {
    @Id
    @Column(name="username")
    private String username;
    @Column(name="code")
    private int code;

    public PasswordRecovery() {
    }

    public PasswordRecovery(String username, int code) {
        this.username = username;
        this.code = code;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}