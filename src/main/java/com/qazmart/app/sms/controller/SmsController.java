package com.qazmart.app.sms.controller;

import com.qazmart.app.sms.entity.PasswordRecovery;
import com.qazmart.app.sms.service.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/sms")
public class SmsController {

    @Autowired
    private SmsService smsService;

    @GetMapping(path = "/add-password-recovery")
    public String addPasswordRecovery(@RequestParam("username") String username, @RequestParam("code") int code) {
        PasswordRecovery passwordRecovery = new PasswordRecovery();
        return smsService.savePasswordRecovery(passwordRecovery);
    }
}