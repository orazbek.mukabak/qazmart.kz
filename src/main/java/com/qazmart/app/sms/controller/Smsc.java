package com.qazmart.app.sms.controller;

import java.net.*;
import java.io.*;
import java.lang.Math;

public class Smsc {
    String SMSC_LOGIN    = "ozat.online";
    String SMSC_PASSWORD = "c94e396cd4e0dd65cd9f3ea78d66a7bf7beed7f2";
    boolean SMSC_HTTPS   = false;
    String SMSC_CHARSET  = "utf-8";
    boolean SMSC_DEBUG   = false;
    boolean SMSC_POST    = false;

    public Smsc() {
    }

    public Smsc(String login, String password) {
        SMSC_LOGIN    = login;
        SMSC_PASSWORD = password;
    }

    public Smsc(String login, String password, String charset) {
        SMSC_LOGIN    = login;
        SMSC_PASSWORD = password;
        SMSC_CHARSET  = charset;
    }

    public Smsc(String login, String password, String charset, boolean debug) {
        SMSC_LOGIN    = login;
        SMSC_PASSWORD = password;
        SMSC_CHARSET  = charset;
        SMSC_DEBUG    = debug;
    }

    public String[] send_sms(String phones, String message, int translit, String time, String id, int format, String sender, String query) {
        String[] formats = {"", "flash=1", "push=1", "hlr=1", "bin=1", "bin=2", "ping=1", "mms=1", "mail=1", "call=1", "viber=1", "soc=1"};
        String[] m = {};
        try {
            m = _smsc_send_cmd("send", "cost=3&phones=" + URLEncoder.encode(phones, SMSC_CHARSET)
                + "&mes=" + URLEncoder.encode(message, SMSC_CHARSET)
                + "&translit=" + translit + "&id=" + id + (format > 0 ? "&" + formats[format] : "")
                + (sender == "" ? "" : "&sender=" + URLEncoder.encode(sender, SMSC_CHARSET))
                + (time == "" ? "" : "&time=" + URLEncoder.encode(time, SMSC_CHARSET) )
                + (query == "" ? "" : "&" + query));
        } catch (UnsupportedEncodingException e) {}
        if (m.length > 1) {
            if (SMSC_DEBUG) {
                if (Integer.parseInt(m[1]) > 0) {
                    System.out.println("");
                }
                else {
                    System.out.print("Ошибка №" + Math.abs(Integer.parseInt(m[1])));
                    System.out.println(Integer.parseInt(m[0])>0 ? (", ID: " + m[0]) : "");
                }
            }
        }
        else {
            System.out.println("");
        }
        return m;
    }

    public String[] get_sms_cost(String phones, String message, int translit, int format, String sender, String query) {
        String[] formats = {"", "flash=1", "push=1", "hlr=1", "bin=1", "bin=2", "ping=1", "mms=1", "mail=1", "call=1", "viber=1", "soc=1"};
        String[] m = {};
        try {
            m = _smsc_send_cmd("send", "cost=1&phones=" + URLEncoder.encode(phones, SMSC_CHARSET)
                + "&mes=" + URLEncoder.encode(message, SMSC_CHARSET)
                + "&translit=" + translit + (format > 0 ? "&" + formats[format] : "")
                + (sender == "" ? "" : "&sender=" + URLEncoder.encode(sender, SMSC_CHARSET))
                + (query == "" ? "" : "&" + query));
        }
        catch (UnsupportedEncodingException e) {
        }
        if (m.length > 1) {
            if (SMSC_DEBUG) {
                if (Integer.parseInt(m[1]) > 0) {
                    System.out.println(m[0] + m[1]);

                } else {
                    System.out.print(Math.abs(Integer.parseInt(m[1])));
                }
            }
        } else {
            System.out.println("");
        }
        return m;
    }

    public String[] get_status(int id, String phone, int all) {
        String[] m = {};
        String tmp;
        try {
            m = _smsc_send_cmd("status", "phone=" + URLEncoder.encode(phone, SMSC_CHARSET) + "&id=" + id + "&all=" + all);
            if (m.length > 1) {
                if (SMSC_DEBUG) {
                    if (m[1] != "" && Integer.parseInt(m[1]) >= 0) {
                        java.sql.Timestamp currentTimestamp = new java.sql.Timestamp(Integer.parseInt(m[1]));
                        System.out.println(m[0]);
                    } else {
                        System.out.println(Math.abs(Integer.parseInt(m[1])));
                    }
                }

                if (all == 1 && m.length > 9 && (m.length < 14 || m[14] != "HLR")) {
                    tmp = _implode(m, ",");
                    m = tmp.split(",", 9);
                }
            } else {
                System.out.println("");
            }

        } catch (UnsupportedEncodingException e) { }
        return m;
    }
    
    public String get_balance() {
        String[] m = {};
        m = _smsc_send_cmd("balance", "");
        if (m.length >= 1) {
            if (SMSC_DEBUG) {
                if(m.length == 1) {
                    System.out.println(m[0]);
                } else {
                    System.out.println(Math.abs(Integer.parseInt(m[1])));
                }
            }
        } else {
            System.out.println("");
        }
        return m.length == 2 ?	"" : m[0];
    }

    private String[] _smsc_send_cmd(String cmd, String arg){
        String ret = ",";
        try {
            String _url = (SMSC_HTTPS ? "https" : "http") + "://smsc.kz/sys/" + cmd +".php?login=" + URLEncoder.encode(SMSC_LOGIN, SMSC_CHARSET)
                + "&psw=" + URLEncoder.encode(SMSC_PASSWORD, SMSC_CHARSET)
                + "&fmt=1&charset=" + SMSC_CHARSET + "&" + arg;

            String url = _url;
            int i = 0;
            do {
                if (i++ > 0) {
                    url = _url;
                    url = url.replace("://smsc.kz/", "://www" + (i) + ".smsc.kz/");
                }
                ret = _smsc_read_url(url);
            }
            while (ret == "" && i < 5);
        }
        catch (UnsupportedEncodingException e) {}
        catch (Exception e) {}
        return ret.split(",");
    }

    private String _smsc_read_url(String url) {
        String line = "", real_url = url;
        String[] param = {};
        boolean is_post = (SMSC_POST || url.length() > 2000);
        if (is_post) {
            param = url.split("\\?",2);
            real_url = param[0];
        }
        try {
            URL u = new URL(real_url);
            InputStream is;

            if (is_post){
                URLConnection conn = u.openConnection();
                conn.setDoOutput(true);
                OutputStreamWriter os = new OutputStreamWriter(conn.getOutputStream(), SMSC_CHARSET);
                os.write(param[1]);
                os.flush();
                os.close();
                System.out.println("post");
                is = conn.getInputStream();
            } else {
                is = u.openStream();
            }
            InputStreamReader reader = new InputStreamReader(is, SMSC_CHARSET);
            int ch;
            while ((ch = reader.read()) != -1) {
                line += (char)ch;
            }
            reader.close();
        }
        catch (MalformedURLException e) { }
        catch (IOException e) { }
        return line;
    }

    private static String _implode(String[] ary, String delim) {
        String out = "";
        for (int i = 0; i < ary.length; i++) {
            if (i != 0)
                out += delim;
            out += ary[i];
        }
        return out;
    }
}