package com.qazmart.app.sms.dao;

import com.qazmart.app.sms.entity.PasswordRecovery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class SmsDAOImplementation implements SmsDAO {

    @Autowired
    private SessionFactory smsSessionFactory;
   
    @Override
    public String savePasswordRecovery(PasswordRecovery passwordRecovery) {
        Session currentSession = smsSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(passwordRecovery);
        return "Done! PasswordRecovery is saved!";
    }

    @Override
    public PasswordRecovery findPasswordRecovery(String username) {
        Session currentSession = smsSessionFactory.getCurrentSession();
        return currentSession.get(PasswordRecovery.class, username);
    }
    
}