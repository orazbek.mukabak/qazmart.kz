package com.qazmart.app.sms.dao;

import com.qazmart.app.sms.entity.PasswordRecovery;

public interface SmsDAO {

    public String savePasswordRecovery(PasswordRecovery passwordRecovery);
    public PasswordRecovery findPasswordRecovery(String username);
    
}