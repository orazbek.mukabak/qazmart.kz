package com.qazmart.app.sms.service;

import com.qazmart.app.sms.entity.PasswordRecovery;

public interface SmsService {
    
    public String savePasswordRecovery(PasswordRecovery passwordRecovery);
    public PasswordRecovery findPasswordRecovery(String username);
}