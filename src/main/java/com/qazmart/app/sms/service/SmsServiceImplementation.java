package com.qazmart.app.sms.service;

import com.qazmart.app.sms.dao.SmsDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qazmart.app.sms.entity.PasswordRecovery;

@Service
public class SmsServiceImplementation implements SmsService {

    @Autowired
    private SmsDAO smsDAO;
    
    @Override
    @Transactional("smsTransactionManager")
    public String savePasswordRecovery(PasswordRecovery passwordRecovery) {
        return smsDAO.savePasswordRecovery(passwordRecovery);
    }

    @Override
    @Transactional("smsTransactionManager")
    public PasswordRecovery findPasswordRecovery(String username) {
         return smsDAO.findPasswordRecovery(username);
    }
}