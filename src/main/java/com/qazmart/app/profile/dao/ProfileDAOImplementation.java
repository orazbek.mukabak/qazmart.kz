package com.qazmart.app.profile.dao;

import com.qazmart.app.profile.entity.Address;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.qazmart.app.profile.entity.Profile;

@Repository
public class ProfileDAOImplementation implements ProfileDAO {

    @Autowired
    private SessionFactory profileSessionFactory;

//    @Override
//    public List<Profile> findAll() {
//        Session currentSession = profileSessionFactory.getCurrentSession();
//        Query<Profile> theQuery = currentSession.createQuery("from Profile order by firstName", Profile.class);
//        List<Profile> profiles = theQuery.getResultList();	
//        return profiles;
//    }

    @Override
    public String save(Profile profile) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(profile);
        return "Done! Profile is saved!";
    }

    @Override
    public Address readAddressByUsername(String username) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        Address address = (Address)currentSession.createQuery("select new Address(username,city,street,house) from Address where username=:username").setString("username", username).uniqueResult();
        return address;
    }

    @Override
    public String createAddress(Address address) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(address);
        return "Done! Address is saved!";
    }

    @Override
    public Profile readProfile(String username) {
        Session currentSession = profileSessionFactory.getCurrentSession();
        Profile profile = (Profile)currentSession.createQuery("select new Profile(username,innerId,firstName,lastName,email,birthDate,regDate) from Profile where username=:username").setString("username", username).uniqueResult();
        return profile;
    }
}