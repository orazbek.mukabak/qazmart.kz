package com.qazmart.app.profile.dao;

import com.qazmart.app.profile.entity.Address;
import com.qazmart.app.profile.entity.Profile;

public interface ProfileDAO {
    public String save(Profile profile);
    public Address readAddressByUsername(String username);
    public String createAddress(Address address);
    public Profile readProfile(String username);
}