package com.qazmart.app.profile.service;

import com.qazmart.app.profile.entity.Address;
import com.qazmart.app.profile.entity.Profile;

public interface ProfileService {
    public String save(Profile profile);
    public Profile createProfile(String username);
    public Address readAddressByUsername(String username);
    public String createAddress(Address address);
    public Profile readProfile(String username);
}