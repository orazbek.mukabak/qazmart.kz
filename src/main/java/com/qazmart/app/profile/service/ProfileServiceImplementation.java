package com.qazmart.app.profile.service;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qazmart.app.profile.dao.ProfileDAO;
import com.qazmart.app.profile.entity.Address;
import com.qazmart.app.profile.entity.Profile;
import java.util.Date;

@Service
public class ProfileServiceImplementation implements ProfileService {

    @Autowired
    private ProfileDAO profileDAO;

    @Override
    @Transactional("profileTransactionManager")
    public String save(Profile profile) {
        return profileDAO.save(profile);
    }

    @Override
    @Transactional("profileTransactionManager")
    public Profile createProfile(String username) {
        Profile profile = profileDAO.readProfile(username);
        if(profile == null) {
            Date regDate = new Date();
            save(new Profile(username, "&#1040;&#1090;&#1099;", "&#1058;&#1077;&#1075;&#1110;", "", regDate, regDate));
            TelegramBot bot = new TelegramBot("1396487174:AAEjxOT5_ltZzVBzsipYkgVULrraRwEoOEQ");
            bot.setUpdatesListener(updates -> {
                return UpdatesListener.CONFIRMED_UPDATES_ALL;
            });
            String message = "NEW USER: " + username;
            SendResponse response = bot.execute(new SendMessage(-437535714, message));
            profile = profileDAO.readProfile(username);
        }
        return profileDAO.readProfile(username);
    }

    @Override
    @Transactional("profileTransactionManager")
    public Address readAddressByUsername(String username) {
        return profileDAO.readAddressByUsername(username);
    }

    @Override
    @Transactional("profileTransactionManager")
    public String createAddress(Address address) {
        return profileDAO.createAddress(address);
    }

    @Override
    @Transactional("profileTransactionManager")
    public Profile readProfile(String username) {
        return profileDAO.readProfile(username);
    }
}