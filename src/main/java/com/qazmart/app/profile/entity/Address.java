package com.qazmart.app.profile.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="address")
public class Address implements Serializable {
    @Id
    @Column(name="username")
    private String username;
    @Column(name="city")
    private String city;
    @Column(name="street")
    private String street;
    @Column(name="house")
    private String house;

    public Address() {
    }

    public Address(String username, String city, String street, String house) {
        this.username = username;
        this.city = city;
        this.street = street;
        this.house = house;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}