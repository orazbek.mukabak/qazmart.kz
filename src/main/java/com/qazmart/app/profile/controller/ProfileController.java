package com.qazmart.app.profile.controller;

import com.qazmart.app.profile.entity.Address;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.qazmart.app.profile.entity.Profile;
import com.qazmart.app.profile.service.ProfileService;
import java.util.ArrayList;
import java.util.Collections;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/profile")
public class ProfileController {

    @Autowired
    private ProfileService profileService;

    @GetMapping(value = "/my-profile")
    public String displayMyProfilePage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String username = authentication.getName();
        Profile profile = profileService.readProfile(username);
        if(profile == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("profile", profile);
            return "profile/my-profile";
        }
    }
    
    @GetMapping(path = "/view/{username:.+}")
    public String displayViewPage(@PathVariable String username, Model model) {
        Profile profile = profileService.readProfile(username);
        if(profile == null) {
            return "exception/exception404";
        } else {
            model.addAttribute("profile", profile);
            return "profile/view";
        }
    }
    
    @GetMapping(value = "/read-address-in-list/{username:.+}")
    public @ResponseBody
    List readAddressInList(@PathVariable String username){
        List<Address> addresses;
        Address address = profileService.readAddressByUsername(username);
        if(address == null) {
            return Collections.emptyList();
        } else {
            addresses = new ArrayList<>();
            addresses.add(address);
            return addresses;
        }
    }
    
    @PostMapping(value = "/create-address-process")
    @ResponseBody
    public String createAddressProcess(@RequestBody Address address) {
        return profileService.createAddress(address);
    }  
    
    @PostMapping(value = "/update-profile")
    @ResponseBody
    public String updateProfile(@RequestBody Profile profile) {
        Profile tempProfile = profileService.readProfile(profile.getUsername());
        tempProfile.setLastName(profile.getLastName());
        tempProfile.setFirstName(profile.getFirstName());
        tempProfile.setEmail(profile.getEmail());
        tempProfile.setBirthDate(profile.getBirthDate());
        return profileService.save(tempProfile);
    } 

    @GetMapping(path = "/edit-basic-info/{username:.+}")
    public String displayEditBasicInfoPage(@PathVariable String username, Model model) {
        Profile profile = profileService.readProfile(username);
        model.addAttribute("profile", profile);
        return "profile/edit-basic-info-jsp";
    }
    
    @GetMapping(path = "/edit-picture/{username:.+}")
    public String displayEditPicturePage(@PathVariable String username, Model model) {
        Profile profile = profileService.readProfile(username);
        model.addAttribute("username", profile.getUsername());
        model.addAttribute("innerId", profile.getInnerId());
        return "profile/edit-picture-jsp";
    }
    
    @GetMapping(value = "/read-profile-in-list/{username:.+}")
    public @ResponseBody
    List readProfileInList(@PathVariable String username){
        List<Profile> profiles;
        Profile profile = profileService.readProfile(username);
        if(profile == null) {
            return Collections.emptyList();
        } else {
            profiles = new ArrayList<>();
            profiles.add(profile);
            return profiles;
        }
    }
}