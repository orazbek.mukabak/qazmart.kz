package com.qazmart.app.cart.entity;

import com.google.gson.Gson;
import java.io.Serializable;
import java.time.LocalDateTime;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="item")
public class Item implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name="id")
    private int id;
    @Column(name="username")
    private String username;
    @Column(name="product_id")
    private int productId;
    @Column(name="title")
    private String title;
    @Column(name="price")
    private double price;
    @Column(name="discount")
    private int discount;
    @Column(name="last_price")
    private double lastPrice;
    @Column(name="quantity")
    private int quantity;
    @Column(name="total_price")
    private double totalPrice;
    @Column(name="img_url")
    private String imgUrl;
    @Column(name="delivered_date_time")
    private LocalDateTime deliveredDateTime;
    @JoinColumn(name="status")
    private char status;
    @ManyToOne(cascade={CascadeType.PERSIST, CascadeType.MERGE, CascadeType.DETACH, CascadeType.REFRESH})
    @JoinColumn(name="position_id")
    private Position position;

    @Transient
    protected Object[] jdoDetachedState;
    
    public Item() {
    }

    public Item(String username, int productId, String title, double price, int discount, double lastPrice, int quantity, double totalPrice, String imgUrl, LocalDateTime deliveredDateTime, char status, Position position) {
        this.username = username;
        this.productId = productId;
        this.title = title;
        this.price = price;
        this.discount = discount;
        this.lastPrice = lastPrice;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.imgUrl = imgUrl;
        this.deliveredDateTime = deliveredDateTime;
        this.status = status;
        this.position = position;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getDiscount() {
        return discount;
    }

    public void setDiscount(int discount) {
        this.discount = discount;
    }

    public double getLastPrice() {
        return lastPrice;
    }

    public void setLastPrice(double lastPrice) {
        this.lastPrice = lastPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public LocalDateTime getDeliveredDateTime() {
        return deliveredDateTime;
    }

    public void setDeliveredDateTime(LocalDateTime deliveredDateTime) {
        this.deliveredDateTime = deliveredDateTime;
    }

    public char getStatus() {
        return status;
    }

    public void setStatus(char status) {
        this.status = status;
    }

    public Position getPosition() {
        return position;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }
}