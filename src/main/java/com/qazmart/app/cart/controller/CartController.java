package com.qazmart.app.cart.controller;

import com.pengrad.telegrambot.TelegramBot;
import com.pengrad.telegrambot.UpdatesListener;
import com.pengrad.telegrambot.request.SendMessage;
import com.pengrad.telegrambot.response.SendResponse;
import com.qazmart.app.cart.entity.Cart;
import com.qazmart.app.cart.entity.Item;
import com.qazmart.app.cart.entity.Position;
import com.qazmart.app.cart.entity.Promo;
import com.qazmart.app.cart.service.CartService;
import com.qazmart.app.product.entity.Product;
import com.qazmart.app.product.service.ProductService;
import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/cart")
public class CartController {
    
    @Autowired
    private CartService cartService;
    
    @Autowired
    private ProductService productService;
    
    @PostMapping(value = "/create-cart")
    @ResponseBody
    public String createCart(@RequestBody Cart cart) {
        Product product = productService.readProductById(cart.getProductId());
        LocalDateTime createdDateTime = LocalDateTime.now();
        cart.setCreatedDateTime(createdDateTime);
        cart.setTitle(product.getTitle());
        cart.setImgUrl(product.getImgUrl());
        cart.setDiscount(product.getDiscount());
        cart.setPrice(product.getPrice());
        cart.setLastPrice(product.getPrice() - product.getPrice()*product.getDiscount()/100);
        cart.setStatus('c');
        return cartService.createCart(cart);
    }
    
    @GetMapping(path = "/count-carts-by-username/{username:.+}")
    public String countCartsByUsername(@PathVariable String username, Model model) {
        long count = cartService.countCartsByUsername(username);
        model.addAttribute("status", count);
        return "status";
    }
    
    @GetMapping(path = "/read-cart")
    public String readCart() {
        return "cart/read-cart";
    }
    
    @GetMapping(path = "/read-carts-by-username/{username:.+}")
    public @ResponseBody
    List readCartsByUsername(@PathVariable String username) {
        return cartService.readCartsByUsernameAndStatus(username, 'c');
    }
    
    @GetMapping(path="/delete-cart/{id:.+}")
    public String deleteCart(@PathVariable int id, Model model) {
        String status = cartService.deleteCart(id);
        model.addAttribute("status", status);
        return "status";
    }
    
    @PostMapping(value = "/create-position")
    @ResponseBody
    public String createPosition(@RequestBody Position position) {
        List<Cart> carts = cartService.readCartsByUsernameAndStatus(position.getUsername(), 'c');
        if(carts != null) {
            double totalCost = 0;
            int totalItem = 0;
            LocalDateTime createdDateTime = LocalDateTime.now();
            position.setCreatedDateTime(createdDateTime);
            position.setStatus('c');
            cartService.createPosition(position);
            for(Cart cart:carts) {
                double totalPrice = 0;
                totalPrice = cart.getLastPrice() * cart.getQuantity();
                totalCost +=totalPrice;
                totalItem +=cart.getQuantity();
                LocalDateTime now = LocalDateTime.now();
                now.plusDays(1);
                Item item = new Item(position.getUsername(), cart.getProductId(), cart.getTitle(), cart.getPrice(), cart.getDiscount(), cart.getLastPrice(), cart.getQuantity(), totalPrice, cart.getImgUrl(), now, 'c', position);
                cartService.createItem(item);
                cart.setStatus('a');
                cartService.createCart(cart);
            }
            position.setTotalCost(totalCost);
            position.setTotalItem(totalItem);
            if(position.getTotalItem() > 0) {
                cartService.createPosition(position);
                TelegramBot bot = new TelegramBot("1396487174:AAEjxOT5_ltZzVBzsipYkgVULrraRwEoOEQ");
                bot.setUpdatesListener(updates -> {
                    return UpdatesListener.CONFIRMED_UPDATES_ALL;
                });
                String message = "NEW ORDER: " + position.getId();
                SendResponse response = bot.execute(new SendMessage(-437535714, message));
                return "STATUS.SUCCESS";
            } else {
                return "STATUS.EMPTY";
            }
        } else {
            return "STATUS.EMPTY";
        }
    }
    
    @GetMapping(path = "/read-positions")
    public String readPositions() {
        return "cart/read-positions";
    }
    
    @GetMapping(path = "/read-positions-by-username/{username:.+}")
    public @ResponseBody
    List readPositionsByUsername(@PathVariable String username) {
        return cartService.readPositionsByUsername(username);
    }
    
    @GetMapping(path = "/read-items/{id:.+}")
    public String readItems(@PathVariable int id, Model model) {
        Position position = cartService.readPosition(id);
        model.addAttribute("position", position);
        return "cart/read-items";
    }
    
    @GetMapping(path = "/update-status/{id:.+}")
    public String updateStatus(@PathVariable int id, @RequestParam("status") char status, Model model) {
        Position position = cartService.readPosition(id);
        position.setStatus(status);
        String output = cartService.createPosition(position);
        model.addAttribute("status", output);
        return "status";
    }
    
    @GetMapping(path = "/read-items-by-position-id/{positionId:.+}")
    public @ResponseBody
    List readItemsByPositionId(@PathVariable int positionId) {
        return cartService.readItemsByPositionId(positionId);
    }
    
    @GetMapping(path = "/read-all-positions")
    public String readAllPositions() {
        return "cart/read-all-positions";
    }
    
//    @GetMapping(value = "/read-promo-in-list/{username:.+}")
//    public @ResponseBody
//    List readAddressInList(@PathVariable String username){
//        List<Promo> addresses;
//        Address address = profileService.readAddressByUsername(username);
//        if(address == null) {
//            return Collections.emptyList();
//        } else {
//            addresses = new ArrayList<>();
//            addresses.add(address);
//            return addresses;
//        }
//    }
}
