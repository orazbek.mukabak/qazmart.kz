package com.qazmart.app.cart.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.qazmart.app.cart.dao.CartDAO;
import com.qazmart.app.cart.entity.Cart;
import com.qazmart.app.cart.entity.Item;
import com.qazmart.app.cart.entity.Position;
import java.util.List;

@Service
public class CartServiceImplementation implements CartService {
    
    @Autowired
    private CartDAO cartDAO;

    @Override
    @Transactional("cartTransactionManager")
    public String createCart(Cart cart) {
        return cartDAO.createCart(cart);
    }

    @Override
    @Transactional("cartTransactionManager")
    public long countCartsByUsername(String username) {
        return cartDAO.countCartsByUsername(username);
    }

    @Override
    @Transactional("cartTransactionManager")
    public String deleteCart(int id) {
        return cartDAO.deleteCart(id);
    }

    @Override
    @Transactional("cartTransactionManager")
    public String createPosition(Position position) {
        return cartDAO.createPosition(position);
    }

    @Override
    @Transactional("cartTransactionManager")
    public String createItem(Item item) {
        return cartDAO.createItem(item);
    }

    @Override
    @Transactional("cartTransactionManager")
    public List<Cart> readCartsByUsernameAndStatus(String username, char status) {
        return cartDAO.readCartsByUsernameAndStatus(username, status);
    }

    @Override
    @Transactional("cartTransactionManager")
    public List<Position> readPositionsByUsername(String username) {
        return cartDAO.readPositionsByUsername(username);
    }

    @Override
    @Transactional("cartTransactionManager")
    public Position readPosition(int id) {
        return cartDAO.readPosition(id);
    }

    @Override
    @Transactional("cartTransactionManager")
    public List<Item> readItemsByPositionId(int positionId) {
        return cartDAO.readItemsByPositionId(positionId);
    }
}

