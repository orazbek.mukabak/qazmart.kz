package com.qazmart.app.cart.service;

import com.qazmart.app.cart.entity.Cart;
import com.qazmart.app.cart.entity.Item;
import com.qazmart.app.cart.entity.Position;
import java.util.List;

public interface CartService {

    public String createCart(Cart cart);
    public long countCartsByUsername(String username);
    public String deleteCart(int id);
    public String createPosition(Position position);
    public String createItem(Item item);
    public List<Cart> readCartsByUsernameAndStatus(String username, char status);
    public List<Position> readPositionsByUsername(String username);
    public Position readPosition(int id);
    public List<Item> readItemsByPositionId(int positionId);
}
