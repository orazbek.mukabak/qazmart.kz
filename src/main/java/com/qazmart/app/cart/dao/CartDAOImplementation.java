package com.qazmart.app.cart.dao;

import com.qazmart.app.cart.entity.Cart;
import com.qazmart.app.cart.entity.Item;
import com.qazmart.app.cart.entity.Position;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CartDAOImplementation implements CartDAO {
    
    @Autowired
    private SessionFactory cartSessionFactory;

    @Override
    public String createCart(Cart cart) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(cart);
        return "Done! Cart is saved!";
    }
    
    @Override
    public long countCartsByUsername(String username) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        long count = (long)currentSession.createQuery("select count(c.id) from Cart c where c.username=:username and c.status='c'").setString("username", username).uniqueResult();
        return count;
    }

    @Override
    public String deleteCart(int id) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        Query theQuery = currentSession.createQuery("delete from Cart where id=:id");
        theQuery.setParameter("id", id);
        theQuery.executeUpdate();
        return "Done! Cart is deleted!";
    }

    @Override
    public String createPosition(Position position) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(position);
        return "Done! Position is saved!";
    }

    @Override
    public String createItem(Item item) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        currentSession.saveOrUpdate(item);
        return "Done! Item is saved!";
    }

    @Override
    public List<Cart> readCartsByUsernameAndStatus(String username, char status) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Cart.class);
        query.add(Restrictions.eq("status", status));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("productId"), "productId")
        .add(Projections.property("quantity"), "quantity")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discount"), "discount")
        .add(Projections.property("lastPrice"), "lastPrice")
        .add(Projections.property("title"), "title")
        .add(Projections.property("imgUrl"), "imgUrl")
        .add(Projections.property("createdDateTime"), "createdDateTime")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Cart.class));
        return query.list();
    }

    @Override
    public List<Position> readPositionsByUsername(String username) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        Criteria query = currentSession.createCriteria(Position.class);
        query.add(Restrictions.eq("username", username));
        query.addOrder(Order.desc("id"));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("totalCost"), "totalCost")
        .add(Projections.property("totalItem"), "totalItem")
        .add(Projections.property("createdDateTime"), "createdDateTime")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Position.class));
        return query.list();
    }

    @Override
    public Position readPosition(int id) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        return currentSession.get(Position.class, id);
    }

    @Override
    public List<Item> readItemsByPositionId(int positionId) {
        Session currentSession = cartSessionFactory.getCurrentSession();
        Position position = currentSession.get(Position.class, positionId);
        Criteria query = currentSession.createCriteria(Item.class);
        query.add(Restrictions.eq("position", position));
        query.setProjection(Projections.projectionList()
        .add(Projections.property("id"), "id")
        .add(Projections.property("username"), "username")
        .add(Projections.property("productId"), "productId")
        .add(Projections.property("title"), "title")
        .add(Projections.property("price"), "price")
        .add(Projections.property("discount"), "discount")
        .add(Projections.property("lastPrice"), "lastPrice")
        .add(Projections.property("quantity"), "quantity")
        .add(Projections.property("totalPrice"), "totalPrice")
        .add(Projections.property("imgUrl"), "imgUrl")
        .add(Projections.property("deliveredDateTime"), "deliveredDateTime")
        .add(Projections.property("status"), "status"))
        .setResultTransformer(Transformers.aliasToBean(Item.class));
        return query.list();
    }
}
