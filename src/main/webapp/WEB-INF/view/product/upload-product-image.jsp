<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel='stylesheet prefetch' href='${pageContext.request.contextPath}/resources/css/croppie.css'>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/upload-product-image.css">
</head>
<body>
    <div class="add-img-wrapper-upload-product-image">
        <label class="cabinet center-block">
            <i class="fas fa-cloud-upload-alt fa-fw-5"></i>Жаңа сурет қосу...
            <input type="file" class="item-img file center-block" name="file_photo" />
        </label>
    </div>
    <div class="modal fade" id="cropImagePop" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                </div>
                <div class="modal-body modal-body-custom">
                    <div id="upload-demo" class="center-block"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Болдырмау</button>
                    <button type="button" id="cropImageBtn" class="btn btn-primary">Жүктеу</button>
                </div>
            </div>
        </div>
    </div>
    <script src="${pageContext.request.contextPath}/resources/js/croppie/croppie.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/product/upload-product-image.js"></script>
    <script>
        initUploadProductImage("${product.id}", "${product.title}", "${pageContext.request.contextPath}");
    </script>
</body>
</html>