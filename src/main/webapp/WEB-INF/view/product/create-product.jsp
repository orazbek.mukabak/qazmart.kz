<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/create-product.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div>
        <div class="title-wrapper-create-product">
            <div class="title-t-create-product">
                Title:
            </div>
            <div class="title-v-create-product">
                <input id="title_create_product" name="title" type="text" class="form-control input-sm" maxlength="255" value="" placeholder="Title..." />
            </div>
        </div>
        <div class="price-wrapper-create-product">
            <div class="price-t-create-product">
                Price:
            </div>
            <div class="price-v-create-product">
                <input id="price_create_product" name="price" type="number" class="form-control input-sm" step="0.01" min="0" max="999999" value="0" />
            </div>
        </div>
        <div class="discount-wrapper-create-product">
            <div class="discount-t-create-product">
                Discount (%):
            </div>
            <div class="discount-v-create-product">
                <input id="discount_create_product" name="discount" type="number" class="form-control input-sm" min="0" max="100" value="0" />
            </div>
        </div>
        <div class="img-url-wrapper-create-product">
            <div class="img-url-t-create-product">
                Image URL:
            </div>
            <div class="img-url-v-create-product">
                <input id="img_url_create_product" name="imgUrl" type="text" class="form-control input-sm" maxlength="127" value="" placeholder="Image URL..." />
            </div>
        </div>
        <div class="d1t-wrapper-create-product">
            <div class="d1t-t-create-product">
                Attribute 1 title:
            </div>
            <div class="d1t-v-create-product">
                <input id="d1t_create_product" name="d1t" type="text" class="form-control input-sm" maxlength="31" value="" placeholder="Attribute 1 title..." />
            </div>
        </div>
        <div class="d1v-wrapper-create-product">
            <div class="d1v-t-create-product">
                Attribute 1 value:
            </div>
            <div class="d1v-v-create-product">
                <input id="d1v_create_product" name="d1v" type="text" class="form-control input-sm" maxlength="127" value="" placeholder="Attribute 1 value..." />
            </div>
        </div>
        <div class="d2t-wrapper-create-product">
            <div class="d2t-t-create-product">
                Attribute 2 title:
            </div>
            <div class="d2t-v-create-product">
                <input id="d2t_create_product" name="d2t" type="text" class="form-control input-sm" maxlength="31" value="" placeholder="Attribute 2 title..." />
            </div>
        </div>
        <div class="d2v-wrapper-create-product">
            <div class="d2v-t-create-product">
                Attribute 2 value:
            </div>
            <div class="d2v-v-create-product">
                <input id="d2v_create_product" name="d2v" type="text" class="form-control input-sm" maxlength="127" value="" placeholder="Attribute 2 value..." />
            </div>
        </div>
        <div class="d3t-wrapper-create-product">
            <div class="d3t-t-create-product">
                Attribute 3 title:
            </div>
            <div class="d3t-v-create-product">
                <input id="d3t_create_product" name="d3t" type="text" class="form-control input-sm" maxlength="31" value="" placeholder="Attribute 3 title..." />
            </div>
        </div>
        <div class="d3v-wrapper-create-product">
            <div class="d3v-t-create-product">
                Attribute 3 value:
            </div>
            <div class="d3v-v-create-product">
                <input id="d3v_create_product" name="d3v" type="text" class="form-control input-sm" maxlength="127" value="" placeholder="Attribute 3 value..." />
            </div>
        </div>
        <div class="full-description-wrapper-create-product">
            <div class="full-description-t-create-product">
                Full description:
            </div>
            <div class="full-description-v-create-product">
                <textarea id="full_description_create_product" name="fullDescription" class="form-control input-sm" rows="5" maxlength="2047" placeholder="Full description..."></textarea>
            </div>
        </div>
        <button class="btn-default" onclick='return saveCreateProduct();'><i class="fas fa-save"></i> Save</button>
    </div>
    <%@ include file="../footer.jsp" %>
    <script>
        function saveCreateProduct() {
            var title = document.getElementById('title_create_product').value;
            var price = document.getElementById('price_create_product').value;
            var discount = document.getElementById('discount_create_product').value;
            var imgUrl = document.getElementById('img_url_create_product').value;
            var d1t = document.getElementById('d1t_create_product').value;
            var d1v = document.getElementById('d1v_create_product').value;
            var d2t = document.getElementById('d2t_create_product').value;
            var d2v = document.getElementById('d2v_create_product').value;
            var d3t = document.getElementById('d3t_create_product').value;
            var d3v = document.getElementById('d3v_create_product').value;
            var fullDescription = document.getElementById('full_description_create_product').value;
            var owner = "${username}";

            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");

            const raw = {title, price, discount, imgUrl, d1t, d1v, d2t, d2v, d3t, d3v, fullDescription, owner };

            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(raw)
            };

            fetch('${pageContext.request.contextPath}/product/create-product-process', requestOptions)
                .then(response => response.text())
                .then(result => {
                    window.location.replace("${pageContext.request.contextPath}/");
                });
        }
    </script>
</body>
</html>