<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/card-view.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/read-products.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div  class="products-read-products" id="products_read_products">
    </div>
    <%@ include file="../footer.jsp" %>
    <script>
        function deleteProduct_ReadProducts(id) {
            fetch('${pageContext.request.contextPath}/product/delete-product/' + id).then(data => {
                $("#product_wrapper_" + id).fadeOut(300);
            });
        }
        
        function updateStatusReadProducts(id, status) {
            fetch('${pageContext.request.contextPath}/product/update-status/' + id + '?status=' + status).then(data => {
                window.location.replace("${pageContext.request.contextPath}/product/read-products-jsp");
            });
        }
        
        function drawSingleProduct_ReadProducts(product) {
            let varHtml = "";
            varHtml += "<div class='product-wrapper' id='product_wrapper_" + product.id + "'>";
            <sec:authorize access="hasAuthority('MANAGER')"> 
                varHtml += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                varHtml += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                if(product.status === 'a') {
                    varHtml += "<button class='dropdown-item' onclick='updateStatusReadProducts(" + product.id + ", \"c\");'><i class='fas fa-eye-slash fa-fw'></i>Көрінбеу</button>";
                } else {
                    varHtml += "<button class='dropdown-item' onclick='updateStatusReadProducts(" + product.id + ", \"a\");'><i class='fas fa-eye fa-fw'></i>Көрсету</button>";
                }
                varHtml += "<button class='dropdown-item' onclick='deleteProduct_ReadProducts(" + product.id + ");'><i class='fas fa-trash-alt fa-fw'></i>Жою</button>";
                varHtml += "</div></div>";
            </sec:authorize>
            if(product.status === 'a') {
                varHtml += "<div class='product-inner-wrapper' onclick='location.href=\"${pageContext.request.contextPath}/product/read-product/" + product.id + "\"'>";
            } else {
                varHtml += "<div class='product-inner-wrapper custom-product-inner-wrapper' onclick='location.href=\"${pageContext.request.contextPath}/product/read-product/" + product.id + "\"'>";
            }
            varHtml += "<img src='https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F" + product.id + "%2F" + product.imgUrl + "?alt=media' onerror=\"this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/product/product.jpg'\" />";
            varHtml += "<div class='product-bottom-wrapper'>";
            varHtml += "<div class='title-product-inner-wrapper'>" + product.title + "</div>";
            let price = product.price-product.price*product.discount/100;
            varHtml += "<div class='product-price'>" + price + "&#8376; (-" + product.discount + "%)</div>";
            varHtml += "<div class='product-initial-price'>" + product.price + "&#8376;</div>";
            varHtml += "</div>";
            varHtml += "</div>";
            varHtml += "</div>";
            return varHtml;  
        }

        function drawProducts_ReadProducts(products) {
            let varHtml = "";
            varHtml += "<div class='products'>";
            for (let i=0; i < products.length; i++) {
                 varHtml += drawSingleProduct_ReadProducts(products[i]);
            }
            varHtml += "</div>";
            document.getElementById('products_read_products').innerHTML = varHtml;
        }
        
        function getProducts_ReadProducts() {
            fetch('${pageContext.request.contextPath}/product/read-products')
                .then(response => response.json())
                .then(data => {
                    drawProducts_ReadProducts(data);
            });
        }
        getProducts_ReadProducts();
    </script>
</body>
</html>