<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/read-product.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/lightbox2/2.11.3/css/lightbox.css" integrity="sha512-Woz+DqWYJ51bpVk5Fv0yES/edIMXjj3Ynda+KWTIkGoynAMHrqTcDUQltbipuiaD5ymEo9520lyoVOo9jCQOCA==" crossorigin="anonymous" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div>
        <div class="wrapper-read-product wrapper-top-read-product">
            <div class="inner-wrapper-read-product inner-wrapper-image-read-product">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" id="images_read_product">
                    <div class="carousel-item active">
                        <a href="https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F${product.id}%2F${product.imgUrl}?alt=media" data-lightbox="img_group" data-title="${product.title}"><img class="d-block w-100" src="https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F${product.id}%2F${product.imgUrl}?alt=media" onerror="this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/product/product.jpg'"></a>
                    </div>
                  </div>
                  <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
            </div>
            <div class="inner-wrapper-read-product inner-wrapper-addition-read-product">
                <div class="inner-wrapper-title-read-product">${product.title}</div>
                <div class="inner-wrapper-rating-read-product">&#9733;&#9733;&#9733;&#9733;&#9733;</div>
                <hr class="hr-read-product">
                <div class="inner-wrapper-price-read-product"><fmt:parseNumber var="intPrice" integerOnly="true" type="number" value="${product.price - product.price * product.discount * 0.01}" />${intPrice}&#8376;</div>
                <div class="inner-wrapper-initial-price-box-read-product">
                    <div class="inner-wrapper-initial-price-read-product"><fmt:parseNumber var="intInitialPrice" integerOnly="true" type="number" value="${product.price}" />${intInitialPrice}&#8376;</div>
                    <div class="inner-wrapper-discount-read-product">-${product.discount}%</div>
                </div>
                <div class="inner-wrapper-quantity-box-read-product">
                    <div class="inner-wrapper-quantity-title-read-product">Саны</div>
                    <div class="inner-wrapper-quantity-read-product"><div class="inner-wrapper-quantity-button-read-product" onclick="setAmountReadProduct(-1);">-</div><div><input class="inner-wrapper-quantity-value-read-product" type="text" step="1" min="1" max="200" value="1" autocomplete="off"></div><div class="inner-wrapper-quantity-button-read-product" onclick="setAmountReadProduct(1);">+</div></div>
                </div>
                <div class="inner-wrapper-purchase-box-read-product">
                    <div class="inner-wrapper-buy-box-read-product"><button class="inner-wrapper-buy-btn-read-product" onclick="buyReadProduct();">Сатып Алу</button></div>
                    <div class="inner-wrapper-add-box-read-product"><button class="inner-wrapper-add-btn-read-product" onclick="addToCartReadProduct();">Кассаға Жөнелту</button></div>
                </div>
            </div>
            <div class="inner-wrapper-read-product inner-wrapper-addition-read-product inner-wrapper-last-read-product">
                <div class="delivery-inner-wrapper-read-product">
                    <img src="${pageContext.request.contextPath}/resources/images/website/delivery.svg" />
                </div>
            </div>
        </div>
        <sec:authorize access="hasAuthority('MANAGER')">
            <div class="wrapper-read-product wrapper-center-read-product wrapper-admin-images-read-product">
                <%@ include file="upload-product-image.jsp" %>
            </div>
            <div class="wrapper-read-product wrapper-center-read-product wrapper-admin-images-read-product" id="admin_images_read_product">
            </div>
        </sec:authorize>
        <div class="wrapper-read-product wrapper-center-read-product">
            <div class="product-details-title-read-product">Тауардың сипаттамасы:</div>
            <div class="product-details-value-read-product">
                <div class="product-details-left-value-read-product">
                    <c:if test="${fn:length(product.d1t) gt 0}">
                        <div>- ${product.d1t}: ${product.d1v}</div>
                    </c:if>
                    <c:if test="${fn:length(product.d3t) gt 0}">
                        <div>- ${product.d3t}: ${product.d3v}</div>
                    </c:if>
                </div>
                <div class="product-details-right-value-read-product">
                    <c:if test="${fn:length(product.d2t) gt 0}">
                        <div>- ${product.d2t}: ${product.d2v}</div>
                    </c:if>
                </div>
            </div>
        </div>
        <div class="wrapper-read-product wrapper-bottom-read-product">
            <div class="product-details-title-read-product">Тауардың толық сипаттамасы:</div>
            <div class="product-details-full-description-read-product">${product.fullDescription}</div>
        </div>
    </div>
    <%@ include file="../footer.jsp" %>
    <script src="${pageContext.request.contextPath}/resources/js/firebase-app.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/firebase-storage.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/watermark.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/lightbox.min.js"></script>
    <script>
        var firebaseConfig = {
                apiKey: "AIzaSyBJCDbde3qauBTiu3aqnlmUg2-YjjLUB-Q",
                authDomain: "qazmartcom.firebaseapp.com",
                storageBucket: "qazmartcom"
        };
        firebase.initializeApp(firebaseConfig);
        
        function setMainImageReadProduct(id, imgUrl, imageId) {
            fetch('${pageContext.request.contextPath}/product/update-product-image/' + id + '?imgUrl=' + imgUrl).then(data => { });
            if(document.getElementsByClassName('admin-main-image-wrapper-read-product')[0]) {
                document.getElementsByClassName('admin-main-image-wrapper-read-product')[0].className = 'admin-image-wrapper-read-product';
            }
            document.getElementById(imageId).className = 'admin-image-wrapper-read-product admin-main-image-wrapper-read-product';
        }
        
        function deleteImageReadProduct(imageId, path) {
            document.getElementById(imageId).style = 'display: none;';
            firebase.storage().ref(path).delete().then(function() { });
        }
        
        function readImagesReadProduct(id) {
            firebase.storage().ref('images/products/' + id).listAll().then(snap => {
                snap.items.forEach(itemRef => {
                    itemRef.getDownloadURL().then(imgUrl => {
                        if(!imgUrl.includes("${product.imgUrl}")) {
                            let varHtml = "<div class='carousel-item'><a href='" + imgUrl + "' data-lightbox='img_group' data-title='${product.title}'><img class='d-block w-100' src='" + imgUrl + "'></a></div>";
                            document.getElementById('images_read_product').innerHTML += varHtml;
                        }
                    });
                });
                <sec:authorize access="hasAuthority('MANAGER')">
                    snap.items.forEach(itemRef => {
                        itemRef.getDownloadURL().then(imgUrl => {
                            let varHtml = "";
                            let imageId = "img_" + itemRef.name + "_read_product";
                            if(imgUrl.includes("${product.imgUrl}")) {
                                varHtml += "<div id='" + imageId + "' class='admin-image-wrapper-read-product admin-main-image-wrapper-read-product'>";
                            } else {
                                varHtml += "<div id='" + imageId + "' class='admin-image-wrapper-read-product'>";
                            }
                            varHtml += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
                            varHtml += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
                            varHtml += "<a class='dropdown-item' href='javascript: setMainImageReadProduct(${product.id},\"" + itemRef.name + "\", \"" + imageId + "\");'><i class='fas fa-home fa-fw'></i>Басты сурет ету</a>";
                            varHtml += "<a class='dropdown-item' href='javascript: deleteImageReadProduct(\"" + imageId + "\", \"" + itemRef.fullPath + "\");'><i class='fas fa-trash-alt fa-fw'></i>Суретті жою</a>";
                            varHtml += "</div></div>";
                            varHtml += "<a href='" + imgUrl + "' data-lightbox='img_group_admin' data-title='${product.title}'><img src='" + imgUrl + "' class='admin-image-wrapper-img-read-product'></a></div>";
                            document.getElementById('admin_images_read_product').innerHTML += varHtml;
                        });
                    });
                </sec:authorize>
            });
        }
        readImagesReadProduct("${product.id}");

        function setAmountReadProduct(change) {
            var input = document.getElementsByClassName("inner-wrapper-quantity-value-read-product")[0];
            var current = parseInt(input.value);
            if(current < 200) {
                input.value = current + change;
                if(input.value < 1) {
                    input.value = 1;
                }
            }
        }
        
        function postCartReadProduct(productId, productTitile, quantity, username, action="add") {
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            const raw = {productId, quantity, username};
            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(raw)
            };
            fetch('${pageContext.request.contextPath}/cart/create-cart', requestOptions)
                .then(response => response.text())
                .then(result => {
                    countCartsByUsername("${username}");
                    if(action === "add") {
                        Swal.fire({
                            position: 'center',
                            type: 'success',
                            title: quantity + " зат кассаға жіберілді...",
                            text: "Атауы: " + productTitile,
                            showConfirmButton: false,
                            timer: 1800
                        });
                    } else {
                        window.location.replace("${pageContext.request.contextPath}/cart/read-cart");
                    }
                });
        }

        function addToCartReadProduct() {
            <sec:authorize access="isAuthenticated()">
                var quantity = document.getElementsByClassName("inner-wrapper-quantity-value-read-product")[0].value;
                postCartReadProduct("${product.id}", "${product.title}", quantity, "${username}");
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                Swal.fire({
                    title: 'Нұсқаулық...',
                    text: "Сатып алу үшін сайтқа тіркеліңіз немесе кіріңіз...",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Кіру',
                    cancelButtonColor: '#f57224',
                    confirmButtonText: 'Тіркелу',
                    confirmButtonColor: '#f57224'
                }).then(function(result) {
                    if (result.value) {
                        window.location.replace("${pageContext.request.contextPath}/register/signup");
                    } else {
                        window.location.replace("${pageContext.request.contextPath}/signin?u=null&p=null");
                    }
                });
            </sec:authorize>
        }
        
        function buyReadProduct() {
            <sec:authorize access="isAuthenticated()">
                var quantity = document.getElementsByClassName("inner-wrapper-quantity-value-read-product")[0].value;
                postCartReadProduct("${product.id}", "${product.title}", quantity, "${username}", "buy");
            </sec:authorize>
            <sec:authorize access="!isAuthenticated()">
                Swal.fire({
                    title: 'Нұсқаулық...',
                    text: "Сатып алу үшін сайтқа тіркеліңіз немесекіріңіз...",
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Кіру',
                    cancelButtonColor: '#f57224',
                    confirmButtonText: 'Тіркелу',
                    confirmButtonColor: '#f57224'
                }).then(function(result) {
                    if (result.value) {
                        window.location.replace("${pageContext.request.contextPath}/register/signup");
                    } else {
                        window.location.replace("${pageContext.request.contextPath}/signin?u=null&p=null");
                    }
                });
            </sec:authorize>
        }
    </script>
</body>
</html>