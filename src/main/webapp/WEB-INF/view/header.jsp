<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link id="dynamic-favicon" rel="shortcut icon" href="${pageContext.request.contextPath}/resources/images/website/favicon.ico" type="image/x-icon" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/header.css" />
</head>
<body>
<nav class="navbar navbar-light bg-light navbar-expand-lg" id="myNavbar">
 
 <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mainNav" aria-controls="mainNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
 </button>
 
 <a href="${pageContext.request.contextPath}/" class="navbar-brand"></a>
 <div class="collapse navbar-collapse" id="mainNav">
  <ul class="navbar-nav ml-auto nav-fill">
    <!--li class="nav-item px-4">
        <a class="nav-link custom-nav-link" href="${pageContext.request.contextPath}/test"><i class="fas fa-spell-check fa-fw-5"></i>Тест</a>
    </li-->
    <li class="nav-item px-4 dropdown">
     <a class="nav-link custom-nav-link dropdown-toggle" href="#" id="servicesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-th fa-fw-5"></i>Каталог</a>
     <div class="dropdown-menu dropdown-menu-right" aria-labelledby="servicesDropdown">
      <div class="d-md-flex align-items-start justify-content-start dropdown-header-wrapper" id="catalogues_header">
      </div>
      <sec:authorize access="hasAuthority('MANAGER')">
         <div class="dropdown-divider"></div>
         <a class="dropdown-item" href="#"><i class="far fa-plus-square fa-fw"></i>Жаңа Каталог</a>
         <a class="dropdown-item" href="${pageContext.request.contextPath}/product/create-product"><i class="far fa-plus-square fa-fw"></i>Жаңа Тауар</a>
         <a class="dropdown-item" href="${pageContext.request.contextPath}/product/read-products-jsp"><i class="fab fa-product-hunt fa-fw"></i>Барлық Тауарлар</a>
      </sec:authorize>
     </div>
    </li>
    <sec:authorize access="!isAuthenticated()">
        <li class="nav-item px-4">
            <a class="nav-link custom-nav-link" href="${pageContext.request.contextPath}/signin?u=null&p=null"><i class="fas fa-sign-in-alt fa-fw-5"></i>Кіру</a>
        </li>
        <li class="nav-item px-4">
            <a class="nav-link custom-nav-link" href="${pageContext.request.contextPath}/register/signup"><i class="fas fa-user-plus fa-fw-5"></i>Тіркелу</a>
        </li>
    </sec:authorize>
    <sec:authorize access="isAuthenticated()">
        <div class="dropdown dropdown-wrpper-header">
            <button type="button" class="btn btn-secondary dropdown-toggle dropdown-custom" data-toggle="dropdown" id="profile_dropdown_header">Профиль</button>
            <div class="dropdown-menu">
                <a class="dropdown-item dropdown-item-custom " href="${pageContext.request.contextPath}/profile/my-profile"><i class="fas fa-user-circle fa-fw"></i>Профильім</a>
                <a class="dropdown-item dropdown-item-custom " href="${pageContext.request.contextPath}/cart/read-positions"><i class="fas fa-boxes fa-fw"></i>Тапсырыстарым</a>
                <sec:authorize access="hasAuthority('MANAGER')">
                    <a class="dropdown-item dropdown-item-custom " href="${pageContext.request.contextPath}/cart/read-all-positions"><i class="fas fa-list-ol fa-fw"></i>Барлық Тапсырыстар</a>
                </sec:authorize>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item dropdown-item-custom " href="${pageContext.request.contextPath}/logout"><i class="fas fa-sign-out-alt fa-fw"></i>Шығу</a>
            </div>
        </div>
        <li class="nav-item px-4">
            <a class="nav-link" href="${pageContext.request.contextPath}/cart/read-cart" id="cart_header"><i class="fas fa-shopping-cart"></i><span class="item-counter-header">0</span></a>
        </li>
    </sec:authorize>
  </ul>
 </div>
</nav>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/7.33.1/sweetalert2.all.min.js"></script>

<script>
    const includeContent = (path, viewPort) => {
        path = path.split(' ').join('%20');
        viewPort = "#" + viewPort;
        $(viewPort).load(path);
    };
    
    function readProfileInList(username) {
        fetch('${pageContext.request.contextPath}/profile/read-profile-in-list/' + username)
            .then(response => response.json())
            .then(data => {
                if(data.length > 0) {
                    let name = data[0].lastName + " " + data[0].firstName;
                    document.getElementById('profile_dropdown_header').innerHTML = name;
                }
        });
    }
    
    function countCartsByUsername(username) {
        fetch('${pageContext.request.contextPath}/cart/count-carts-by-username/' + username)
            .then(response => response.json())
            .then(data => {
                document.getElementsByClassName("item-counter-header")[0].innerHTML = data;
                $("#cart_header").fadeOut(100).fadeIn(200);
        });
    }
    <sec:authorize access="isAuthenticated()">
        countCartsByUsername("${username}");
        readProfileInList("${username}");
    </sec:authorize>
    
    function drawSingleMainCatalogue_Header(catalogue) {
        let varHtml = "";
        varHtml += "<div>";
        varHtml += "<div class='dropdown-header'>" + catalogue.title + "</div>";
        varHtml += "<a href='${pageContext.request.contextPath}/catalogue/read-main-catalogue/" + catalogue.id + "'><img class='dropdown-header-img' src='https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fcatalogue%2F" + catalogue.id + ".jpg?alt=media' onerror=\"this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/catalogue/catalogue.jpg'\" alt='" + catalogue.title + "'></a>";
        //varHtml += "<a class='dropdown-item' href='#'>Websites</a>";
        varHtml += "</div>";
        return varHtml;
    }
    
    function drawSingleMainCatalogueFooter_Header(catalogue) {
        let varHtml = "";
        varHtml += "<li>";
            varHtml += "<a href='${pageContext.request.contextPath}/catalogue/read-main-catalogue/" + catalogue.id + "'>" + catalogue.title + "</a>";
        varHtml += "</li>";
        return varHtml;
    }
    
    function drawMainCatalogues_Header(catalogues) {
        let varHtml = "";
        let varHtmlFooter = "";
        for (let i=0; i < catalogues.length; i++) {
            varHtml += drawSingleMainCatalogue_Header(catalogues[i]);
            varHtmlFooter += drawSingleMainCatalogueFooter_Header(catalogues[i]);
        }
        document.getElementById('catalogues_header').innerHTML = varHtml;
        document.getElementById('catalogues_footer_header').innerHTML = varHtmlFooter;
    }
    
    function getMainCatalogues_Header() {
        fetch('${pageContext.request.contextPath}/catalogue/read-main-catalogues')
            .then(response => response.json())
            .then(data => {
                drawMainCatalogues_Header(data);
        });
    }
    getMainCatalogues_Header();
</script>
</body>
</html>