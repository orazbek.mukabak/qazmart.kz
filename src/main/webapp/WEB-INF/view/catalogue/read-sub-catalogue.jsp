<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/card-view.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/catalogue/view.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/catalogue/read-sub-catalogue.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div class="wrapper-read-sub-catalogue">
        <div class="sub-menu" id="sub_catalogue_read_sub_catalogue">
        </div>
        <div  class="products-read-sub-catalogue" id="products_read_sub_catalogue">
        </div>
    </div>
    <%@ include file="../footer.jsp" %>
    <script>
        function drawSingleProductByMainSubCatalogueId_ReadSubCatalogue(product) {
            let varHtml = "";
            varHtml += "<div class='product-wrapper' id='product_wrapper_" + product.id + "'>";
            varHtml += "<div class='product-inner-wrapper' onclick='location.href=\"${pageContext.request.contextPath}/product/read-product/" + product.id + "\"'>";
            varHtml += "<img src='https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F" + product.id + "%2F" + product.imgUrl + "?alt=media' onerror=\"this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/product/product.jpg'\" />";
            varHtml += "<div class='product-bottom-wrapper'>";
            varHtml += "<div class='title-product-inner-wrapper'>" + product.title + "</div>";
            let price = product.price-product.price*product.discount/100;
            varHtml += "<div class='product-price'>" + price + "&#8376; (-" + product.discount + "%)</div>";
            varHtml += "<div class='product-initial-price'>" + product.price + "&#8376;</div>";
            varHtml += "</div>";
            varHtml += "</div>";
            varHtml += "</div>";
            return varHtml;  
        }

        function drawProductsByMainSubCatalogueId_ReadSubCatalogue(products) {
            let varHtml = "";
            varHtml += "<div class='products'>";
            for(let i=0; i < products.length; i++) {
                if(products[i].status === 'a') {
                    varHtml += drawSingleProductByMainSubCatalogueId_ReadSubCatalogue(products[i]);
                }
            }
            varHtml += "</div>";
            document.getElementById('products_read_sub_catalogue').innerHTML = varHtml;
        }
        
        function getProductsByMainSubCatalogueId_ReadSubCatalogue(id) {
            fetch('${pageContext.request.contextPath}/product/read-products-by-main-sub-catalogue-id/' + id + '?subCatalogueId=${subCatalogue.id}')
                .then(response => response.json())
                .then(data => {
                    drawProductsByMainSubCatalogueId_ReadSubCatalogue(data);
            });
        }
        getProductsByMainSubCatalogueId_ReadSubCatalogue("${subCatalogue.mainCatalogueId}");

        function updateSubCatalogueDesign_ReadSubCatalogue() {
            if(window.location.href.includes("read-main-catalogue")) {
                document.getElementById('sub_catalogue_0_read_sub_catalogue').classList.add("active");
            } else {
                let id = window.location.href.split("/"); 
                id = id[id.length - 1];
                document.getElementById('sub_catalogue_' + id + '_read_sub_catalogue').classList.add("active");
            }
        }

        function drawSingleSubCatalogueByMainCatalogueId_ReadSubCatalogue(catalogue) {
            let varHtml = "";
            varHtml += "<a href='${pageContext.request.contextPath}/catalogue/read-sub-catalogue/" + catalogue.id + "' id='sub_catalogue_" + catalogue.id + "_read_sub_catalogue'>" + catalogue.title + "</a>";
            return varHtml;
        }

        function drawSubCataloguesByMainCatalogueId_ReadSubCatalogue(catalogues) {
            let varHtml = "";
            varHtml += "<a href='${pageContext.request.contextPath}/catalogue/read-main-catalogue/${subCatalogue.mainCatalogueId}' id='sub_catalogue_0_read_sub_catalogue'>Барлығы</a>";
            if(catalogues.length > 0) {
                for(let i=0; i < catalogues.length; i++) {
                    varHtml += drawSingleSubCatalogueByMainCatalogueId_ReadSubCatalogue(catalogues[i]);
                }
            }
            document.getElementById('sub_catalogue_read_sub_catalogue').innerHTML = varHtml;
        }
        
        function getSubCataloguesByMainCatalogueId_ReadSubCatalogue(id) {
            fetch('${pageContext.request.contextPath}/catalogue/read-sub-catalogues-by-main-catalogue-id/' + id)
                .then(response => response.json())
                .then(data => {
                    drawSubCataloguesByMainCatalogueId_ReadSubCatalogue(data);
                    updateSubCatalogueDesign_ReadSubCatalogue();
            });
        }
        getSubCataloguesByMainCatalogueId_ReadSubCatalogue("${subCatalogue.mainCatalogueId}");
    </script>
</body>
</html>