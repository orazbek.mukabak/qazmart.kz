<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/signup.css" />
</head>
<body>
    <div class="register-page-signup-jsp">
        <div class="register-wrap-signup-jsp">
            <div class="register-title-signup-jsp">
                Құпия Сөзді Қалпына Келтіру!
            </div>
            <div class="register-content-signup-jsp">
                <form:form action="${pageContext.request.contextPath}/register/recoverying" modelAttribute="authUser" onsubmit="return checkPasswordRecoveryJsp(this);">
                    <div>
                        <c:if test="${registrationError != null}">
                            <script>
                                var error = '${registrationError}';
                                if(error === 'ERROR_USER_DO_NOT_EXISTS') {
                                    error = 'Кешіріңіз, телефон нөміріңіз осы сайтта тіркелген.';
                                } else if(error === 'ERROR_EMPTY_USERNAME') {
                                    error = 'Кешіріңіз, телефон нөміріңізді жазыңыз.';
                                } else if(error === 'ERROR_EMPTY_PASSWORD') {
                                    error = 'Кешіріңіз, жаңа құпия сөзді жазыңыз.';
                                } else {
                                    error = 'Кешіріңіз, жаңа құпия сөз 6-32 ұзындығында болуы керек.';
                                }
                                Swal.fire({
                                    title: 'Қайта тырысып көріңіз...',
                                    text: error,
                                    confirmButtonColor: '#4B2D73'
                                });
                            </script>
                        </c:if>                                         
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-user"></i>
                        <form:input id="register_phone_signup_jsp" path="userName" placeholder="Телефон" class="form-control" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password_recovery_jsp" path="password" placeholder="Жаңа құпия сөз (минимум 6 орын)" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password2_recovery_jsp" path="" placeholder="Жаңа құпия сөзді қайталау" class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <button type="submit" class="btn register-btn-signup-jsp">SMS-код жіберу</button>
                </form:form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/lib/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        $(function(){
          $("#register_phone_signup_jsp").mask("8 (999) 999-99-99");
        });
        
        function checkPasswordRecoveryJsp(form) {
          if(form.password_recovery_jsp.value !== form.password2_recovery_jsp.value) {
                Swal.fire({
                    title: 'Қайта тырысып көріңіз...',
                    text: 'Құпия сөз дұрыс жазылмады.',
                    confirmButtonColor: '#4B2D73'
                });
              form.password_recovery_jsp.focus();
              return false;
            }
          return true;
        }
    </script>
</body>
</html>