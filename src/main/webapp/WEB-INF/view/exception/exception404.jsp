<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/error.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div class="wrapper-page-404">
        <img src="${pageContext.request.contextPath}/resources/images/website/404.svg" class="image-page-404" />
    </div>
    <%@ include file="../footer.jsp" %>
</body>
</html>