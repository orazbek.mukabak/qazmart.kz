<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<sec:authorize access="isAuthenticated()">
    <sec:authentication property="principal.username" var="username" />
</sec:authorize>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/product/card-view.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/index.css" />
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet"/>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
</head>
<body>
    <%@ include file="header.jsp" %>
    <%@ include file="banner.jsp" %>
    <div id='all_products'>
    </div>
    <%@ include file="footer.jsp" %>
    <script>
        function drawSingleProduct_Index(product) {
            let varHtml = "";
            varHtml += "<div class='product-wrapper' id='product_wrapper_" + product.id + "'>";
            varHtml += "<div class='product-inner-wrapper' onclick='location.href=\"${pageContext.request.contextPath}/product/read-product/" + product.id + "\"'>";
            varHtml += "<img src='https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F" + product.id + "%2F" + product.imgUrl + "?alt=media' onerror=\"this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/product/product.jpg'\" />";
            varHtml += "<div class='product-bottom-wrapper'>";
            varHtml += "<div class='title-product-inner-wrapper'>" + product.title + "</div>";
            let price = product.price-product.price*product.discount/100;
            varHtml += "<div class='product-price'>" + price + "&#8376; (-" + product.discount + "%)</div>";
            varHtml += "<div class='product-initial-price'>" + product.price + "&#8376;</div>";
            varHtml += "</div>";
            varHtml += "</div>";
            varHtml += "</div>";
            return varHtml;
        }

        function drawProducts_Index(products) {
            let varHtml = "";
            varHtml += "<div class='products'>";
            for (let i=0; i < products.length; i++) {
                if(products[i].status === 'a') {
                    varHtml += drawSingleProduct_Index(products[i]);
                }
            }
            varHtml += "</div>";
            document.getElementById('all_products').innerHTML = varHtml;
        }
        
        function getProducts_Index() {
            fetch('${pageContext.request.contextPath}/product/read-products')
                .then(response => response.json())
                .then(data => {
                    drawProducts_Index(data);
            });
        }
        getProducts_Index();
    </script>
</body>
</html>