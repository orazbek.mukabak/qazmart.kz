<%@ page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
</head>
<body>
<footer class="page-footer font-small stylish-color-dark pt-4">
  <div class="container text-center text-md-left">
    <div class="row">
      <div class="col-md-4 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">QAZMART</h5>
        <p>QAZMART - бұл оңайлылық...</p>
      </div>
      <hr class="clearfix w-100 d-md-none">
      <div class="col-md-2 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">КАТАЛОГ</h5>
        <ul class="list-unstyled" id="catalogues_footer_header">
        </ul>
      </div>
      <hr class="clearfix w-100 d-md-none">
      <sec:authorize access="isAuthenticated()">
      <div class="col-md-2 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">ҚОЛДАНУШЫ</h5>
        <ul class="list-unstyled">
          <li>
            <a href="${pageContext.request.contextPath}/profile/my-profile">Профильім</a>
          </li>
          <li>
            <a href="${pageContext.request.contextPath}/cart/read-cart">Кассаға Өту</a>
          </li>
          <li>
            <a href="${pageContext.request.contextPath}/cart/read-positions">Тапсырыстарым</a>
          </li>
        </ul>
      </div>
      </sec:authorize>
      <hr class="clearfix w-100 d-md-none">
      <div class="col-md-2 mx-auto">
        <h5 class="font-weight-bold text-uppercase mt-3 mb-4">БІЗ ТУРАЛЫ</h5>
        <ul class="list-unstyled">
          <li>
            <a href="#">Бізбен Байланысу</a>
          </li>
          <li>
            <a href="#">Команда Туралы</a>
          </li>
        </ul>
      </div>
    </div>
  </div>
  <hr>
    <sec:authorize access="!isAuthenticated()">
  <ul class="list-unstyled list-inline text-center py-2">
    <li class="list-inline-item">
      <h5 class="mb-1">Сайытқа тіркеліп, ұнатқан затыңызға тапсырыс беріңіз...</h5>
    </li>
    <li class="list-inline-item">
      <a href="${pageContext.request.contextPath}/register/signup" class="btn btn-success btn-rounded"><i class="fas fa-user-plus fa-fw-5"></i>Тіркелу</a>
    </li>
  </ul>
  <hr>
    </sec:authorize>
  <ul class="list-unstyled list-inline text-center">
    <li class="list-inline-item">
      <a class="btn-floating btn-fb mx-1">
        <i class="fab fa-facebook-f"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-tw mx-1">
        <i class="fab fa-twitter"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-gplus mx-1">
        <i class="fab fa-google-plus-g"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-li mx-1">
        <i class="fab fa-linkedin-in"> </i>
      </a>
    </li>
    <li class="list-inline-item">
      <a class="btn-floating btn-dribbble mx-1">
        <i class="fab fa-dribbble"> </i>
      </a>
    </li>
  </ul>
  <div class="footer-copyright text-center py-3">© 2020 QAZMART ECOMMERCE PLATFORM
  </div>
</footer>
</body>
</html>