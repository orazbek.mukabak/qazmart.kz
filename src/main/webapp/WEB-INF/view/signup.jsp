<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="header.jsp" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/signup.css" />
</head>
<body>
    <div class="register-page-signup-jsp">
        <div class="register-wrap-signup-jsp">
            <div class="register-title-signup-jsp" id='register_title_signup'>
                QAZMART-қа Тіркеліңіз де, Қалаған Тауарыңызға Тапсырыс Беріңіз...
            </div>
            <div class="register-content-signup-jsp">
                <form:form action="${pageContext.request.contextPath}/register/signing" modelAttribute="authUser" onsubmit="return checkPasswordSignupJsp(this);">
                    <div>
                        <c:if test="${registrationError != null}">
                            <script>
                                var error = '${registrationError}';
                                if(error === 'ERROR_USER_ALREADY_EXISTS') {
                                    error = 'Сіз әлде қашан тіркеліп қойғансыз, кіру бетіне өтіңіз...';
                                } else if(error === 'ERROR_EMPTY_USERNAME') {
                                    error = 'Телефон номеріңізді жазыңыз...';
                                } else {
                                    error = 'Пароль ең аз деген де 6, ал ең көп деген де 32 таңбадан болуы қажет...';
                                }
                                Swal.fire({
                                    title: 'Қайта тырысып көріңіз...',
                                    text: error,
                                    type: 'warning',
                                    confirmButtonColor: '#f57224'
                                });
                            </script>
                        </c:if>                                         
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-user"></i>
                        <form:input id="register_phone_signup_jsp" path="userName" placeholder="Телефон..." class="form-control" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password_signup_jsp" path="password" placeholder="Пароль..." class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>
                    <div class="register-input-wrapper-signup-jsp">
                        <i class="fas fa-lock"></i>
                        <form:password id="password2_signup_jsp" path="" placeholder="Парольді қайталаңыз..." class="form-control" pattern="[A-Za-z0-9].{5,32}" title="Құпия сөз тек латын әріптері және сандар болуы керек. Ең аз болғанда 6 орын, ең көп болғанда 32 орын болуы керек!" />
                    </div>

                    <button type="submit" class="btn register-btn-signup-jsp" id='register_btn_signup'>Тіркелу</button>
                    <div class="register-login-wrapper-signup-jsp">
                        <span id='already_registered'>Сіз тіркеліп қойғансыз ба?</span>
                        <a id='login_signup' href="${pageContext.request.contextPath}/signin?u=null&p=null">Кіру</a>
                    </div>
                </form:form>
            </div>
        </div>
    </div>
    <%@ include file="footer.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/lib/jquery.maskedinput.min.js"></script>
    <script type="text/javascript">
        $(function(){
          $("#register_phone_signup_jsp").mask("8 (999) 999-99-99");
        });

        function checkPasswordSignupJsp(form) {
          if(form.password_signup_jsp.value !== form.password2_signup_jsp.value) {
                Swal.fire({
                    title: 'Қайта тырысып көріңіз...',
                    text: 'Пароль дұрыс жазылмады',
                    type: 'warning',
                    confirmButtonColor: '#f57224'
                });
              form.password_signup_jsp.focus();
              return false;
            }
          return true;
        }
    </script>
</body>
</html>