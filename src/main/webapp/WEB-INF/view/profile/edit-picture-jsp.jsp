<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/croppie.css" />
        <title>QazMart</title>
                <script src="${pageContext.request.contextPath}/resources/js/firebase-app.js"></script>
        <script>
            var firebaseConfig = {
                apiKey: "AIzaSyBJCDbde3qauBTiu3aqnlmUg2-YjjLUB-Q",
                authDomain: "qazmartcom.firebaseapp.com",
                storageBucket: "qazmartcom"
            };
            firebase.initializeApp(firebaseConfig);
        </script>
    </head>
    <body>
        <div class="upload-demo-wrap">
            <div id="upload-demo"></div>
        </div>
        <input class="file file-btn" id="inputPictureEditPictureJsp" type="file" accept="image/*">
        <input type="text" id="innerId" value="${innerId}" style="display: none;"/>
        <button class="btn-default btn-margin-top" id="btnSavePictureEditPictureJsp"><i class="fas fa-save"></i> Save</button>
        <button class="btn-default btn-margin-top" onclick="window.location.replace('${pageContext.request.contextPath}/profile/my-profile');"><i class="fas fa-ban"></i> Cancel</button>

        <script src="${pageContext.request.contextPath}/resources/js/firebase-storage.js"></script>
        <script src="https://apis.google.com/js/platform.js" async defer></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>');</script>
        <script src="${pageContext.request.contextPath}/resources/js/croppie/croppie.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/croppie/exif.js"></script>
        <script src="${pageContext.request.contextPath}/resources/js/profile/upload-profile-image.js"></script>
        <script>
            CropImage.init("${innerId}", "${username}");
        </script>
    </body>
</html>