<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>QazMart</title>
    </head>
    <body>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Тегі:
            </div>
            <div class="profile-user-info-item-value">
                <input id="lastNameEditBasicInfoJsp" type="text" class="form-control input-sm" value="${profile.lastName}" />
            </div>
        </div>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Аты:
            </div>
            <div class="profile-user-info-item-value">
                <input id="firstNameEditBasicInfoJsp" type="text" class="form-control input-sm" value="${profile.firstName}" />
            </div>
        </div>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Электронды пошта:
            </div>
            <div class="profile-user-info-item-value">
                <input id="emailEditBasicInfoJsp" type="email" class="form-control input-sm" value="${profile.email}" />
            </div>
        </div>
        <div class="profile-user-info-item">
            <div class="profile-user-info-item-name">
                Туылған күні:
            </div>
            <div class="profile-user-info-item-value">
                <input id="birthDateEditBasicInfoJsp" type="date" class="input-sm date-edit-basic-info" value="<fmt:formatDate pattern='yyyy-MM-dd' value='${profile.birthDate}' />">
            </div>
        </div>
        <div class="save-my-profile">
            <button class="btn-default btn-margin-top" onclick='return updateEditBasicInfo();'><i class="fas fa-save"></i> Save</button>
            <button class="btn-default btn-margin-left" onclick="window.location.replace('${pageContext.request.contextPath}/profile/my-profile');"><i class="fas fa-ban"></i> Cancel</button>
        </div>
        <script>
            function updateEditBasicInfo() {
                var username = "${username}";
                var lastName = document.getElementById('lastNameEditBasicInfoJsp').value;
                var firstName = document.getElementById('firstNameEditBasicInfoJsp').value;
                var email = document.getElementById('emailEditBasicInfoJsp').value;
                var birthDate = document.getElementById('birthDateEditBasicInfoJsp').value;

                let myHeaders = new Headers();
                myHeaders.append("Content-Type", "application/json");

                const raw = {username, lastName, firstName, email, birthDate};

                let requestOptions = {
                    method: 'POST',
                    headers: myHeaders,
                    body: JSON.stringify(raw)
                };

                fetch('${pageContext.request.contextPath}/profile/update-profile', requestOptions)
                    .then(response => response.text())
                    .then(result => {
                        window.location.replace("${pageContext.request.contextPath}/profile/my-profile");
                    });
            }
        </script>
    </body>
</html>