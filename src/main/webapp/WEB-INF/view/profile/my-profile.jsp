<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>QazMart</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/profile/my-profile.css" />
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css" />
    </head>
    <body>
        <%@ include file="../header.jsp" %>
        <div class="profile-wrapper-my-profile">
            <div id="profile_user_image_container" class="profile-left-my-profile">
                <div class="left-image-box-my-profile">
                    <img id="imgViewPictureJsp" src="https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fprofile%2F${profile.innerId}.jpg?alt=media&time=<%=System.currentTimeMillis()%>" onerror="this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/profile/profile.jpg'" />
                    <button class="btn-default btn-default-flat btn-margin-top" onclick='return includeContent("${pageContext.request.contextPath}/profile/edit-picture/${profile.username}", "profile_user_image_container");'><i class="fas fa-edit fa-fw-5"></i>Өңдеу</button>
                </div>
                <div class="left-bottom-box-my-profile">
                    Сайтқа тіркелді: ${profile.regDate}
                </div>
            </div>
            <div id="profile_user_info_container" class="profile-right-my-profile">
                <div class="profile-user-info-item">
                    <div class="profile-user-info-item-name">
                        Аккаунт №:
                    </div>
                    <div class="profile-user-info-item-value">
                        ${profile.innerId}
                    </div>
                </div>
                <div class="profile-user-info-item">
                    <div class="profile-user-info-item-name">
                        Тегі:
                    </div>
                    <div class="profile-user-info-item-value">
                        ${profile.lastName}
                    </div>
                </div>
                <div class="profile-user-info-item">
                    <div class="profile-user-info-item-name">
                        Аты:
                    </div>
                    <div class="profile-user-info-item-value">
                        ${profile.firstName}
                    </div>
                </div>
                <div class="profile-user-info-item">
                    <div class="profile-user-info-item-name">
                        Ұялы телефон:
                    </div>
                    <div class="profile-user-info-item-value">
                        ${profile.username}
                    </div>
                </div>
                <div class="profile-user-info-item">
                    <div class="profile-user-info-item-name">
                        Электронды пошта:
                    </div>
                    <div class="profile-user-info-item-value">
                        ${profile.email}
                    </div>
                </div>
                <div class="profile-user-info-item">
                    <div class="profile-user-info-item-name">
                        Туылған күні:
                    </div>
                    <div class="profile-user-info-item-value">
                        <fmt:formatDate type="date" value="${profile.birthDate}" />
                    </div>
                </div>
                <div class="save-my-profile">
                    <button class="btn-default btn-margin-top" onclick='return includeContent("${pageContext.request.contextPath}/profile/edit-basic-info/${profile.username}", "profile_user_info_container");'><i class="fas fa-edit fa-fw-5"></i>Өңдеу</button>
                </div>
            </div>
        </div>
        <%@ include file="../footer.jsp" %>
    </body>
</html>