<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/cart/read-cart.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div class="wrapper-read-cart">
        <div class="wrapper-left-read-cart" id="wrapper_left_read_cart">
            <i class="fas fa-shopping-cart fa-fw-5"> Кассаға ешқандай тауар келіп түскен жоқ...</i>
        </div>
        <div class="wrapper-right-read-cart">
            <div class="right-box-read-cart">
                <div class="location-box-read-cart">
                    <div class="location-box-left-read-cart">
                        <div class="location-title-read-cart">Жеткізіп беру орны</div>
                        <div class="location-value-read-cart" id="location_value_read_cart"></div>
                    </div>
                    <div class="location-box-right-read-cart">
                        <a href="javascript: updateAddress_ReadCart();">Өзгерту</a>
                    </div>
                </div>
                <div class="contact-read-cart">Байланыс: ${username}</div>
                <hr class="hr-read-cart">
                <div class="order-box-read-cart">
                    <div class="order-title-read-cart">Тапсырыс Мәліметтері</div>
                    <div class="subtotal-box-read-cart">
                        <div class="subtotal-items-read-cart" id="subtotal_items_read_cart">Тауардың саны (0 зат)</div>
                        <div class="subtotal-cost-read-cart" id="subtotal_cost_read_cart">0&#8376;</div>
                    </div>
                    <div class="delivery-box-read-cart">
                        <div class="delivery-title-read-cart">Жеткізу құны</div>
                        <div class="delivery-cost-read-cart">0&#8376;</div>
                    </div>
                    <div class="voucher-box-read-cart">
                        <div class="voucher-input-box-read-cart">
                            <input class="voucher-input-read-cart" type="text" placeholder="Промокод" value="" height="100%" id="promo_code_read_cart">
                        </div>
                        <div class="voucher-button-box-read-cart">
                            <button class="voucher-button-read-cart" type="button" onclick="readPromoCodeReadCart();">Тексеру</button>
                        </div>
                    </div>
                    <div class="total-box-read-cart">
                        <div class="total-title-read-cart">Төлеу Керек Сумма</div>
                        <div class="total-value-read-cart" id="total_value_read_cart">0&#8376;</div>
                    </div>
                    <div class="gst-read-cart">0% ҚҚС(НДС)</div>
                    <div class="gst-read-cart">
                        <button type="button" class="checkout-read-cart" onclick="postPositionReadCart();">ТАПСЫРЫС БЕРУ</button>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <%@ include file="../footer.jsp" %>
    <script>
        function readPromoCodeReadCart() {
            let code = document.getElementById('promo_code_read_cart').value;
            if(code === "NEWUSER3000") {
                Swal.fire({
                    position: 'center',
                    type: 'success',
                    title: "Жеңідік",
                    text: "Сізге 3000 тг жеңілдік жасалатын болады...",
                    showConfirmButton: false,
                    timer: 10000
                });
            }
       //     fetch('${pageContext.request.contextPath}/cart/read-promo-in-list/' + code)
        //        .then(response => response.json())
         //       .then(data => {
         //           if(data.length > 0) {
         //               //drawCartsByUsername_ReadCart(data);
         //           }
          //  });
        }
        
        function postPositionReadCart() {
            let username = "${username}";
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            const raw = {username};
            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(raw)
            };
            fetch('${pageContext.request.contextPath}/cart/create-position', requestOptions)
                .then(response => response.text())
                .then(result => {
                    if(result.includes('STATUS.EMPTY')) {
                        Swal.fire({
                            position: 'center',
                            type: 'warning',
                            title: "Касса бос...",
                            text: "Кассаға ешқандай тауар келіп түскен жоқ...",
                            showConfirmButton: false,
                            timer: 1800
                        });
                    } else {
                        window.location.replace("${pageContext.request.contextPath}/cart/read-positions");
                    }
                });
        }
        
        function deleteCart_ReadCart(id) {
            $("#cart_" + id + "_read_cart").fadeOut(300);
            fetch('${pageContext.request.contextPath}/cart/delete-cart/' + id).then(data => {
                countCartsByUsername("${username}");
                updateTotal_ReadCart("${username}");
            });
        }
        
        function postAddress_ReadCart(address) {
            let myHeaders = new Headers();
            myHeaders.append("Content-Type", "application/json");
            let username = "${username}";
            let city = address[0];
            let street = address[1];
            let house = address[2];

            const raw = { username, city, street, house };

            let requestOptions = {
                method: 'POST',
                headers: myHeaders,
                body: JSON.stringify(raw)
            };
            fetch('${pageContext.request.contextPath}/profile/create-address-process', requestOptions)
                .then(response => response.text())
                .then(result => {
                    getAddressByUsername_ReadCart("${username}");
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: "Сәтті өңделді...",
                        text: "Жеткізіп беру орны: " + city + ", " + street + " " + house,
                        showConfirmButton: false,
                        timer: 1800
                    });
                });
        }

        function updateAddress_ReadCart() {
            let city = document.getElementById('city_read_cart').innerHTML;
            let street = document.getElementById('street_read_cart').innerHTML;
            let house = document.getElementById('house_read_cart').innerHTML;
            (async() => {
            const { value: formValues } = await Swal.fire({
                html:
                '<div class="change-address-input-title-read-cart">Қала</div>' +
                '<select class="change-address-input-read-cart" id="swal-input1"><option value="Нұр-Сұлтан" selected>Нұр-Сұлтан</option></select>' +
                '<div class="change-address-input-title-read-cart">Көше</div>' +
                '<input class="change-address-input-read-cart" value="' + street + '" id="swal-input2">' +
                '<div class="change-address-input-title-read-cart">Үй/Офис</div>' +
                '<input class="change-address-input-read-cart" value="' + house + '" id="swal-input3">',
                focusConfirm: false,
                confirmButtonColor: '#f57224',
                confirmButtonText: '<i class="fas fa-save fa-fw-5"></i> Сақтау',
                preConfirm: () => {
                    return [
                        document.getElementById('swal-input1').value,
                        document.getElementById('swal-input2').value,
                        document.getElementById('swal-input3').value
                    ];
                }
            });
            if(formValues) {
                postAddress_ReadCart(formValues);
            }
            })();
        }
        
        function drawAddress_ReadCart(address) {
            let varHtml = "<i class='fas fa-map-marker-alt'></i> ";
            if(address.city.length > 0) {
                varHtml += "<i class='address-line-read-cart' id='city_read_cart'>" + address.city + "</i>, ";
            }
            if(address.street.length > 0) {
                varHtml += "<i class='address-line-read-cart' id='street_read_cart'>" + address.street + "</i> ";
            }
            varHtml += "<i class='address-line-read-cart' id='house_read_cart'>" + address.house + "</i>";
            document.getElementById('location_value_read_cart').innerHTML = varHtml;
        }

        function getAddressByUsername_ReadCart(username) {
            fetch('${pageContext.request.contextPath}/profile/read-address-in-list/' + username)
                .then(response => response.json())
                .then(data => {
                    if(data.length > 0) {
                        drawAddress_ReadCart(data[0]);
                    } else {
                        let varHtml = "<i class='fas fa-map-marker-alt'></i> ";
                        varHtml = "Жеткізіп беру орнын жазыңыз...";
                        document.getElementById('location_value_read_cart').innerHTML = varHtml;
                    }
            });
        }
        getAddressByUsername_ReadCart("${username}");
        
        function updateDrawTotal_ReadCart(subTotal, total) {
            document.getElementById('subtotal_items_read_cart').innerHTML = "Тауардың саны (" + subTotal + " зат)";
            document.getElementById('subtotal_cost_read_cart').innerHTML = total + "&#8376;";
            document.getElementById('total_value_read_cart').innerHTML = total + "&#8376;";
            $("#subtotal_items_read_cart").fadeOut(100).fadeIn(200);
            $("#subtotal_cost_read_cart").fadeOut(100).fadeIn(200);
            $("#total_value_read_cart").fadeOut(100).fadeIn(200);
        }
        
        function updateTotal_ReadCart(username) {
            fetch('${pageContext.request.contextPath}/cart/read-carts-by-username/' + username)
                .then(response => response.json())
                .then(data => {
                    let subTotal = 0;
                    let total = 0;
                    if(data.length > 0) {
                        for (let i=0; i < data.length; i++) {
                            subTotal += data[i].quantity;
                            total += (data[i].price - (data[i].price * data[i].discount / 100)) * data[i].quantity;
                        }
                    }
                    updateDrawTotal_ReadCart(subTotal, total);
            });
        }
        
        function drawSingleCart_ReadCart(cart) {
            let varHtml = "";
            varHtml += "<div class='cart-wrapper-read-cart' id='cart_" + cart.id + "_read_cart'>";
                varHtml += "<div class='cart-image-box-read-card'>";
                    varHtml += "<img class='cart-image-read-card' src='https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F" + cart.productId + "%2F" + cart.imgUrl + "?alt=media' onerror=\"this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/product/product.jpg'\">";
                varHtml += "</div>";
                varHtml += "<div class='cart-text-box-read-card'>";
                    varHtml += "<div class='cart-title-read-card'>" + cart.title + "</div>";
                    varHtml += "<hr class='hr-custom-read-cart'>";
                    varHtml += "<div class='cart-bottom-box'>";
                        varHtml += "<div class='cart-bottom-box-left-wrapper-read-card'>";
                            varHtml += "<div class='cart-bottom-box-left-read-card'>";
                                varHtml += "<div class='cart-price-read-card'>" + cart.price + "&#8376;</div>";
                                varHtml += "<div class='cart-discount-read-card'>-" + cart.discount + "%</div>";
                            varHtml += "</div>";
                            varHtml += "<div class='cart-bottom-box-left-read-card'>";
                                varHtml += "<div class='cart-last-price-read-card'>" + (cart.price - cart.price * cart.discount / 100) + "&#8376;</div>";
                                varHtml += "<div class='cart-last-price-multiply-read-card'>&#10005;</div>";
                                varHtml += "<div class='cart-last-price-read-card'>" + cart.quantity + "</div>";
                                varHtml += "<div class='cart-last-price-multiply-read-card'>=</div>";
                                varHtml += "<div class='cart-sum-last-price-read-card'>" + (cart.price - cart.price * cart.discount / 100) * cart.quantity + "&#8376;</div>";
                            varHtml += "</div>";
                        varHtml += "</div>";
                        varHtml += "<div class='cart-bottom-box-right-wrapper-read-card'>";
                        varHtml += "<a href='javascript: deleteCart_ReadCart(" + cart.id + ");'><i class='fas fa-trash-alt'></i></a>";
                        varHtml += "</div>";
                    varHtml += "</div>";
                varHtml += "</div>";
            varHtml += "</div>";
            return varHtml;  
        }
        
        function drawCartsByUsername_ReadCart(carts) {
            let subTotal = 0;
            let total = 0;
            let varHtml = "";
            for (let i=0; i < carts.length; i++) {
                varHtml += drawSingleCart_ReadCart(carts[i]);
                subTotal += carts[i].quantity;
                total += (carts[i].price - (carts[i].price * carts[i].discount / 100)) * carts[i].quantity;
            }
            document.getElementById('wrapper_left_read_cart').innerHTML = varHtml;
            updateDrawTotal_ReadCart(subTotal, total);
        }
        
        function readCartsByUsername_ReadCart(username) {
            fetch('${pageContext.request.contextPath}/cart/read-carts-by-username/' + username)
                .then(response => response.json())
                .then(data => {
                    if(data.length > 0) {
                        drawCartsByUsername_ReadCart(data);
                    }
            });
        }
        readCartsByUsername_ReadCart("${username}");
    </script>
</body>
</html>