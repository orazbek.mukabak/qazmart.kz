<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/cart/read-items.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div class="wrapper-read-items">
        <div class="wrapper-position-read-items">
            <div class="position-id-read-items">
                <div class="position-id-title-read-items">
                    Тапсырыс
                </div>
                <div class="position-id-value-read-items">
                    ${position.id}
                </div>
            </div>
            <div class="position-created-date-read-items">
                <div class="position-created-date-title-read-items">
                    Уақыт
                </div>
                <div class="position-created-date-value-read-items">
                    ${position.createdDateTime.dayOfMonth} ${position.createdDateTime.month} ${position.createdDateTime.year} ${position.createdDateTime.hour}:${position.createdDateTime.minute}
                </div>
            </div>
        </div>
        <hr class="hr-read-items">     
        <div class="wrapper-status-read-items">
            <div class="container container-custom">
                <div class="card-custom">
                    <!--div class="row d-flex justify-content-between px-3 top">
                        <div class="d-flex">
                            <h5>Тапсырыс <span class="text-primary font-weight-bold">${position.id}</span></h5>
                        </div>
                        <div class="d-flex flex-column text-sm-right">
                            <p class="mb-0">Жеткізілетін уақыт <span>01/12/19</span></p>
                        </div>
                    </div--> <!-- Add class 'active' to progress -->
                    <div class="row d-flex justify-content-center">
                        <div class="col-12">
                            <ul id="progressbar_read_items" class="text-center">
                            </ul>
                        </div>
                    </div>
                    <div class="row justify-content-between top">
                        <div class="row d-flex icon-content"> <img class="icon" src="${pageContext.request.contextPath}/resources/images/website/created.svg">
                            <div class="d-flex flex-column">
                                <p class="font-weight-bold">Тапсырыс<br>берілді</p>
                            </div>
                        </div>
                        <div class="row d-flex icon-content"> <img class="icon" src="${pageContext.request.contextPath}/resources/images/website/confirmed.svg">
                            <div class="d-flex flex-column">
                                <p class="font-weight-bold">Тапсырыс<br>мақұлданды</p>
                            </div>
                        </div>
                        <div class="row d-flex icon-content"> <img class="icon" src="${pageContext.request.contextPath}/resources/images/website/in-transit.svg">
                            <div class="d-flex flex-column">
                                <p class="font-weight-bold">Тапсырыс<br>жеткізілуде</p>
                            </div>
                        </div>
                        <div class="row d-flex icon-content"> <img class="icon" src="${pageContext.request.contextPath}/resources/images/website/delivered.svg">
                            <div class="d-flex flex-column">
                                <p class="font-weight-bold">Тапсырыс<br>жеткізілді</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <hr class="hr-read-items">
        <div class="wrapper-items-read-items" id="wrapper_items_read_items">
        </div>
    </div>
    <%@ include file="../footer.jsp" %>
    <script>
        function readPositionStatus_ReadItems() {
            let varHtml = "";
            if("${position.status}" === "c") {
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='step0'></li>";
                varHtml += "<li class='step0'></li>";
                varHtml += "<li class='step0'></li>";
            } else if("${position.status}" === "y") {
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='step0'></li>";
                varHtml += "<li class='step0'></li>";
            } else if("${position.status}" === "t") {
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='step0'></li>";
            } else if("${position.status}" === "d") {
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='active step0'></li>";
                varHtml += "<li class='active step0'></li>";
            } else {
                varHtml += "<li class='step0'></li>";
                varHtml += "<li class='step0'></li>";
                varHtml += "<li class='step0'></li>";
                varHtml += "<li class='step0'></li>";
            }
            document.getElementById("progressbar_read_items").innerHTML = varHtml;
        }
        readPositionStatus_ReadItems();
        
        function drawSingleItem_ReadItems(item) {
            let varHtml = "";
            varHtml += "<div class='item-wrapper-read-items'>";
                varHtml += "<div class='image-box-read-items'>";
                    varHtml += "<img class='image-read-items' src='https://firebasestorage.googleapis.com/v0/b/qazmartcom/o/images%2Fproducts%2F" + item.productId + "%2F" + item.imgUrl + "?alt=media' onerror=\"this.onerror=null; this.src='${pageContext.request.contextPath}/resources/images/product/product.jpg'\">";
                varHtml += "</div>";
                varHtml += "<div class='item-text-box-read-items'>";
                    varHtml += "<div class='item-title-read-items'>" + item.title + "</div>";
                    varHtml += "<hr class='hr-custom-read-items'>";
                    varHtml += "<div class='item-bottom-box-read-items'>";
                        varHtml += "<div class='item-price-read-items'>" + item.lastPrice + "&#8376;</div>";
                        varHtml += "<div class='item-quantity-read-items'>Саны: " + item.quantity + "</div>";
                        varHtml += "<div class='item-total-price-read-items'>" + item.totalPrice + "&#8376;</div>";
                    varHtml += "</div>";
                    varHtml += "<div class='item-bottom-box-read-items item-bottom-box-custom-read-items'>";
                        if(item.status === 'y') {
                            varHtml += "<div class='item-status-confirmed-read-items'>Мақұлданды</div>";
                        } else if(item.status === 't') {
                            varHtml += "<div class='item-status-transit-read-items'>Жеткізілуде</div>";
                        } else if(item.status === 'd') {
                            varHtml += "<div class='item-status-delivered-read-items'>Жеткізілді</div>";
                        } else if(item.status === 'x') {
                            varHtml += "<div class='item-status-cancelled-read-items'>Тоқтатылды</div>";
                        } else {
                            varHtml += "<div class='item-status-created-read-items'>Тапсырыс берілді</div>";
                        }
                        if(item.status === 'y' || item.status === 't' || item.status === 'c') {
                            varHtml += "<div class='item-delivery-date-time-read-items'>";
                            varHtml += "Жеткізіледі: ";
                            if(item.deliveredDateTime.date) {
                                varHtml += item.deliveredDateTime.date.day + " ";
                                varHtml += item.deliveredDateTime.date.month + " ";
                                varHtml += item.deliveredDateTime.date.year + " ";
                                varHtml += item.deliveredDateTime.time.hour + ":";
                                varHtml += item.deliveredDateTime.time.minute;
                            } else {
                                varHtml += item.deliveredDateTime.dayOfMonth + " ";
                                varHtml += item.deliveredDateTime.month + " ";
                                varHtml += item.deliveredDateTime.year + " ";
                                varHtml += item.deliveredDateTime.hour + ":";
                                varHtml += item.deliveredDateTime.minute;
                            }
                            varHtml += "</div>";
                        }
                    varHtml += "</div>";
                varHtml += "</div>";
            varHtml += "</div>";
            return varHtml;  
        }
        
        function drawItemsByPositionId_ReadItems(items) {
            let varHtml = "";
            for (let i=0; i < items.length; i++) {
                varHtml += drawSingleItem_ReadItems(items[i]);
            }
            document.getElementById('wrapper_items_read_items').innerHTML = varHtml;
        }
        
        function readItemsByPositionId_ReadItems(id) {
            fetch('${pageContext.request.contextPath}/cart/read-items-by-position-id/' + id)
                .then(response => response.json())
                .then(data => {
                    if(data.length > 0) {
                        drawItemsByPositionId_ReadItems(data);
                    }
            });
        }
        readItemsByPositionId_ReadItems("${position.id}");
    </script>
</body>
</html>