<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/cart/read-positions.css" />
</head>
<body>
    <%@ include file="../header.jsp" %>
    <div id="wrapper_read_positions">
    </div>
    <%@ include file="../footer.jsp" %>
    <script>
        function viewDetailsReadPositions(id) {
            window.location.replace("${pageContext.request.contextPath}/cart/read-items/" + id);
        }
        
        function updateStatusReadPositions(id, status) {
            fetch('${pageContext.request.contextPath}/cart/update-status/' + id + '?status=' + status).then(data => {
                window.location.replace("${pageContext.request.contextPath}/cart/read-positions");
            });
        }
        
        function drawSinglePosition_ReadPositions(position) {
            let varHtml = "";
            varHtml += "<div class='position-wrapper-read-positions'>";
                varHtml += "<div class='position-header-read-positions'>";
                    varHtml += "<div class='position-id-read-positions'>";
                        varHtml += "<div class='position-id-title-read-positions'>";
                            varHtml += "Тапсырыс";
                        varHtml += "</div>";
                        varHtml += "<div class='position-id-value-read-positions'>";
                            varHtml += position.id;
                        varHtml += "</div>";
                    varHtml += "</div>";
                    varHtml += "<div class='position-created-date-read-positions'>";
                        varHtml += "<div class='position-created-date-title-read-positions'>";
                            varHtml += "Уақыт";
                        varHtml += "</div>";
                        varHtml += "<div class='position-created-date-value-read-positions'>";
                            varHtml += position.createdDateTime.dayOfMonth + " ";
                            varHtml += position.createdDateTime.month + " ";
                            varHtml += position.createdDateTime.year + " ";
                            varHtml += position.createdDateTime.hour + ":";
                            varHtml += position.createdDateTime.minute;
                        varHtml += "</div>";
                    varHtml += "</div>";
                varHtml += "</div>";
                varHtml += "<hr class='hr-read-positions'>";
                varHtml += "<div class='position-body-read-positions'>";
                    if(position.status === 'y') {
                        varHtml += "<div class='position-status-confirmed-read-positions'>Мақұлданды</div>";
                    } else if(position.status === 't') {
                        varHtml += "<div class='position-status-transit-read-positions'>Жеткізілуде</div>";
                    } else if(position.status === 'd') {
                        varHtml += "<div class='position-status-delivered-read-positions'>Жеткізілді</div>";
                    } else if(position.status === 'x') {
                        varHtml += "<div class='position-status-cancelled-read-positions'>Тоқтатылды</div>";
                    } else {
                        varHtml += "<div class='position-status-created-read-positions'>Тапсырыс берілді</div>";
                    }
                    varHtml += "<div class='position-total-item-read-positions'>";
                        varHtml += "Саны: " + position.totalItem + " тауар";
                    varHtml += "</div>";
                    varHtml += "<div class='position-total-cost-read-positions'>";
                        varHtml += position.totalCost + "&#8376;";
                    varHtml += "</div>";
                    varHtml += "<div class='position-button-read-positions'>";
                        if(position.status === 'c' || position.status === 'y') {
                            varHtml += "<button class='position-cancel-read-positions' onclick='updateStatusReadPositions(" + position.id + ", \"x\");'>Отмена</button>";
                        }
                        if(position.status === 'x') {
                            varHtml += "<button class='position-delete-read-positions' onclick='updateStatusReadPositions(" + position.id + ", \"n\");'>Жою</button>";
                        }
                        varHtml += "<button class='position-view-details-read-positions' onclick='viewDetailsReadPositions(" + position.id + ");'>Толық Ақпарат</button>";
                    varHtml += "</div>";
                varHtml += "</div>";
            varHtml += "</div>";
            return varHtml;  
        }
        
        function drawPositionsByUsername_ReadPositions(positions) {
            let varHtml = "";
            for (let i=0; i < positions.length; i++) {
                if(positions[i].status !== 'n') {
                    varHtml += drawSinglePosition_ReadPositions(positions[i]);
                }
            }
            document.getElementById('wrapper_read_positions').innerHTML = varHtml;
        }
        
        function readPositions_ReadAllPositions() {
            fetch('${pageContext.request.contextPath}/cart/read-positions-by-username/' + username)
                .then(response => response.json())
                .then(data => {
                    if(data.length > 0) {
                        drawPositionsByUsername_ReadPositions(data);
                    }
            });
        }
        readPositions_ReadAllPositions();
    </script>
</body>
</html>