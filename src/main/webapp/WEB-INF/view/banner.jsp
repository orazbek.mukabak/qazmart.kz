<%@ page contentType="text/html" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>QazMart</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/banner.css" />
</head>
<body>
<section style="position: sticky;">
    <div id="carouselExampleFade" class="carousel slide carousel-fade" data-ride="carousel">
        <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="${pageContext.request.contextPath}/resources/images/banner/1.jpg" class="d-block w-100">
            </div>
            <div class="carousel-item">
                <img src="${pageContext.request.contextPath}/resources/images/banner/2.jpg" class="d-block w-100">
            </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleFade" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleFade" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>
<section class="search-sec">
    <div class="container">
        <div class="search-row-banner">
            <div class="search-input-wrapper">
                <input type="text" class="form-control search-slt" placeholder="Іздеу..." value="" id="keyword_value_banner">
            </div>
            <div class="search-btn-wrapper">
                <button type="button" class="btn btn-danger wrn-btn" onclick="readProductsByKeyword();"><i class="fas fa-search fa-fw-5"></i>Іздеу</button>
            </div>
        </div>
    </div>
</section>
<script>
    function readProductsByKeyword() {
        let keyword = document.getElementById('keyword_value_banner').value;
        fetch('${pageContext.request.contextPath}/product/read-products-by-keyword?keyword=' + keyword)
            .then(response => response.json())
            .then(data => {
                if(data.length > 0) {
                    drawProducts_Index(data);
                } else {
                    let varHtml = "<div class='keyword-not-found-banner'>";
                    varHtml += "Сіз енгізген <b>" + keyword + "</b> сөзіне байланысты ешқандай мәлімет табылмады, басқаша жазып іздеп көріңіз...";
                    varHtml += "</div>";
                    document.getElementById('all_products').innerHTML = varHtml;
                }
        });
    }

    var keywordValueBanner = document.getElementById("keyword_value_banner");
    keywordValueBanner.addEventListener("keyup", function(event) {
        if(event.keyCode === 13) {
            readProductsByKeyword();
        }
    });
</script>
</body>
</html>