var $uploadCrop,
tempFilename,
rawImg,
imageId;

function uploadImageToFirebaseReadProduct(file, path){
    var storageRef = firebase.storage().ref(path);
    var uploadTask = storageRef.put(file);
    var subscribe = uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED);
    subscribe({
        'next': function(snapshot) {
        },
        'error': function(error) {
        },
        'complete': function() {
            uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
               // window.location.replace("my-profile");
            });
        }
    });
}

function insertToAdminImagesReadProduct(fileName, src, path, productId, productTitle) {
    var reader = new FileReader();
    reader.readAsDataURL(src); 
    reader.onloadend = function() {
        var base64src = reader.result;
        let varHtml = "";
        let imageId = "img_" + fileName + "_read_product";
        varHtml += "<div id='" + imageId + "' class='admin-image-wrapper-read-product'>";
        varHtml += "<div><a class='nav-link dropdown-toggle'  style='float: right;' href='' role='button' data-toggle='dropdown' aria-expanded='false'><i class='fa fa-cogs' aria-hidden='true'></i></a>";
        varHtml += "<div class='dropdown-menu dropdown-menu-right' aria-labelledby='navbarDropdown'>";
        varHtml += "<a class='dropdown-item' href='javascript: setMainImageReadProduct(" + productId + ",\"" + fileName + "\", \"" + imageId + "\");'><i class='fas fa-home fa-fw'></i>&#x411;&#x430;&#x441;&#x442;&#x44b; &#x441;&#x443;&#x440;&#x435;&#x442; &#x435;&#x442;&#x443;</a>";
        varHtml += "<a class='dropdown-item' href='javascript: deleteImageReadProduct(\"" + imageId + "\", \"" + path + "\");'><i class='fas fa-trash-alt fa-fw'></i>&#x421;&#x443;&#x440;&#x435;&#x442;&#x442;&#x456; &#x436;&#x43e;&#x44e;</a>";
        varHtml += "</div></div>";
        varHtml += "<a href='" + base64src + "' data-lightbox='img_group_admin' data-title='" + productTitle + "'><img src='" + base64src + "' class='admin-image-wrapper-img-read-product'></a></div>";
        document.getElementById('admin_images_read_product').innerHTML += varHtml;
    };
}

function readFile(input, contextPath) {
   // if(input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('.upload-demo').addClass('ready');
            $('#cropImagePop').modal('show');

            var my_watermarked = new Watermark();
            my_watermarked.setPicture(e.target.result, [1200, 600]).addWatermark(contextPath + '/resources/images/src/watermark.png').render( function(){
                var resulting_data_urls = my_watermarked.getDataUrls('image/png', 1);
                rawImg=resulting_data_urls[2];
            });
            
        };
        reader.readAsDataURL(input.files[0]);

  //  } else {
  //      swal("Sorry, something went wrong...");
  //  }
}

function initUploadProductImage(productId, productTitle, contextPath) {
    $uploadCrop = $('#upload-demo').croppie({
        viewport: {
            width: 600,
            height: 400
        },
        enforceBoundary: false,
        enableExif: true
    });

    $('#cropImagePop').on('shown.bs.modal', function(){
        $uploadCrop.croppie('bind', {
            url: rawImg
        }).then(function(){ });
    });

    $('.item-img').on('change', function () { imageId = $(this).data('id'); tempFilename = $(this).val();
    $('#cancelCropBtn').data('id', imageId); readFile(this, contextPath); });
    $('#cropImageBtn').on('click', function (ev) {
        $uploadCrop.croppie('result', {
            type: 'blob',
            format: 'jpeg',
            backgroundColor: '#fff',
            quality: 0.9,
            size: {
                width: 600, height: 400
            }
        }).then(function (resp) {
            //$('#item-img-output').attr('src', resp);
            $('#cropImagePop').modal('hide');
            var filename = Math.floor(Math.random() * 99999999999) + ".jpg";
            var path = '/images/products/' + productId + '/' + filename;
            insertToAdminImagesReadProduct(filename, resp, path, productId, productTitle);
            uploadImageToFirebaseReadProduct(resp, path);
        });
    });
}